import fetch from 'node-fetch';

const baseAPIdev = 'http://localhost:3701/api'; //End point para obtener coordenadas de prueba STRN
const baseAPIprod = 'http://78.46.16.8:3700/api'; //Endpoint para insertar en giz_transmision

const splitToBulks = (arr, bulkSize = 20) => {
    const bulks = [];
    for (let i = 0; i < Math.ceil(arr.length / bulkSize); i++) {
        bulks.push(arr.slice(i * bulkSize, (i + 1) * bulkSize));
    }
    return bulks;
}

const login = async () => {
    const params = new URLSearchParams();
    params.append('usuario', '20438933272');
    params.append('contrasena', '1a2b3c++');

    const response = await fetch(`${baseAPIprod}/users/login`, { method: 'POST', body: params });
    const { token } = await response.json();
    return token;
}

const getTransmissions = async () => {
    const response = await fetch(`${baseAPIdev}/transmissions`);
    const transmisiones = await response.json();
    return transmisiones;
}

const postTransmission = async (token, body) => {
    try {
        await fetch(`${baseAPIprod}/transmissions`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            }
        });
    } catch (error) {
        console.log(`Error: ${error}`)
    }
}

console.log(`Solicitando token de acceso`);

const token = await login();
const transmissions = await getTransmissions();

console.log(`token: ${token}`);
console.log(`usuario: 20438933272 contraseña: 1a2b3c++`);

splitToBulks(transmissions, 200).forEach(async (trx, index) => {
    try {
        setTimeout(async () => {
            await postTransmission(token, trx);
            console.log(`Se insertaron ${trx.length} coordenadas del bloque ${index}`);
        }, 500 * (index + 1));
    } catch (error) {
        console.error(`Internal Error: ${error}`);
    }
});