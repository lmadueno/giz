# Agente de coordenadas

Se encarga de recepcionar e insertar las coordenadas en giz_transmision.

## Instalación

Ir al directorio api-agent e instalar las dependencias

```bash
 cd backend/api-agent
 npm install
```

## Crear archivo .ENV con la siguiente estructura

```
#APP
APP_PORT=3500

#PG DB
PG_HOST=localhost
PG_PORT=5432
PG_USER=postgres
PG_PASSWORD=sasa
PG_NAME=giz

#JWT
SECRET=agent_giz_2021$$

#QUEUE
QUEUE_HOST=localhost
QUEUE_EXCHANGE=giz
QUEUE_EXCHANGE_TYPE=direct
QUEUE_NAME=giz_transmision
QUEUE_ROUTING_KEY=giz_transmision_key
```

## Desarrollo

```bash
 cd backend/api-agent
 npm run dev
```

## Producción

```bash
 cd backend/api-agent
 pm2 start agent.js
```
