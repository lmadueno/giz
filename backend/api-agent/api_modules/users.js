const helper = require("../helper/jwt");
const pg = require('../services/pg');

/**
 * Login
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.login = async (req, res) => {
    if (!req.body.usuario || !req.body.contrasena)
        return res.status(400).send({ message: 'Debe enviar el usuario y contraseña de la EMV' });

    try {
        const user = await pg.query(`
        select 
            id_emv as id, 
            login_usuario as usuario, 
            password_usuario as contrasena
        from giz.giz_emv 
        where 
            login_usuario = $1`,
            [req.body.usuario]);

        if (!user[0])
            return res.status(400).send({ message: 'Credenciales de acceso incorrectas' });

        if (!helper.comparePassword(user[0].contrasena, req.body.contrasena))
            return res.status(400).send({ 'message': 'Credenciales de acceso incorrectas' });

        const token = helper.generateToken(user[0].id);

        await pg.query(`update giz.giz_emv set token = $1 where id_emv = $2`, [token, user[0].id]);

        return res.status(200).send({ token });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Refresh token
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.refreshToken = async (req, res) => {
    if (!req.body.loginUsuario)
        return res.status(400).send({ message: 'Debe enviar usuario de la EMV' });

    try {
        const user = await pg.query(`
        select 
            id_emv as id, 
            login_usuario as usuario, 
            password_usuario as contrasena
        from giz.giz_emv 
        where 
            login_usuario = $1`,
            [req.body.loginUsuario]);

        if (!user[0])
            return res.status(400).send({ message: 'Credenciales de acceso incorrectas' });

        const token = helper.generateToken(user[0].id);

        await pg.query(`update giz.giz_emv set token = $1 where id_emv = $2`, [token, user[0].id]);

        return res.status(200).send({ token });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}