const pg = require('../services/pg');
const queue = require('../api_modules/queue');

exports.getAll = async (req, res) => {
    try {
        const trx = await pg.query(`
        select 
         latitud, 
         longitud, 
         velocidad, 
         orientacion, 
         to_char(fecha_emv, 'dd/mm/yyyy HH24:MI:SS') as fecha_emv, 
         id_emv, 
         placa_vehiculo, 
         id_evento 
         from giz_test.data_coordenadas 
         limit 10000`);
        return res.status(200).send(trx);
    } catch (error) {
        res.status(500).json({ message: error });
    }
}

/**
 * Insert a new block of transmissions
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.insert = async (req, res) => {
    const body = req.body;

    if (!Array.isArray(body))
        res.status(400).json({ message: "Debe enviar un arreglo con al menos una coordenada" });

    if (body.length === 0)
        res.status(400).json({ message: "Debe enviar al menos una coordenada" });

    try {
        await queue.publish(
            body.map(row => ({ ...row, id_emv: req.user.id }))
        );
        res.status(201).json({ message: "Registro exitoso" });
    } catch (erroarrayr) {
        res.status(500).json({ message: error });
    }
}