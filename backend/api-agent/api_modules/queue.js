const amqp = require('amqplib');
const config = require('../config/queue');

const HOST = config.host,
    EXCHANGE = config.exchange,
    EXCHANGE_TYPE = config.type,
    QUEUE = config.name,
    ROUTING_KEY = config.routingKey;

const runAmqp = async () => {
    const connection = await amqp.connect(`amqp://${HOST}`);
    const channel = await connection.createChannel();

    const commonOptions = {
        durable: false
    };

    await channel.assertExchange(EXCHANGE, EXCHANGE_TYPE, commonOptions);
    await channel.assertQueue(QUEUE, commonOptions);
    await channel.bindQueue(QUEUE, EXCHANGE, ROUTING_KEY, commonOptions);
    await channel.close();

    return connection;
};

module.exports.publish = async (msg) => {
    const connection = await runAmqp();
    const channel = await connection.createChannel();
    await channel.publish(EXCHANGE, ROUTING_KEY, new Buffer(JSON.stringify(msg)), {
        noAck: true
    });
    await channel.close();
    console.log(`Message sent: ${JSON.stringify(msg)}`);
}