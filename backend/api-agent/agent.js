require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
const cluster = require('cluster');

const PORT = process.env.APP_PORT;
const numCPUs = require('os').cpus().length;

const auth = require("./middleware/auth");
const users = require("./api_modules/users");
const transmissions = require("./api_modules/transmissions");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get('/', (req, res) => { res.send('Agente de coordenadasX') });
app.post('/api/users/login', users.login);
app.post('/api/users/refresh-token', users.refreshToken);
app.get('/api/transmissions', transmissions.getAll);
app.post('/api/transmissions', auth.verifyToken, transmissions.insert);

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    app.listen(PORT, err => {
        err ?
            console.log("Error in server setup") :
            console.log(`Worker ${process.pid} started`);
    });
}