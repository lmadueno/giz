# RabbitMQ subscriber

Consumir la cola de mensajes de transmisiones e invocar a function fi_transmision

## Instalación

Ir al directorio queue e instalar las dependencias

```bash
 cd backend/queue
 npm install
```

## Desarrollo

```bash
 cd backend/queue
 npm run dev
```

## Producción

```bash
 cd backend/queue
 pm2 start subscriber.js
```
