const amqp = require('amqplib');
const format = require('pg-format');
const pg = require('./services/pg');
const queueConfig = require('./config/queue');

const HOST = queueConfig.host,
    EXCHANGE = queueConfig.exchange,
    EXCHANGE_TYPE = queueConfig.type,
    QUEUE = queueConfig.name,
    ROUTING_KEY = queueConfig.routingKey;

const runAmqp = async () => {
    const connection = await amqp.connect(`amqp://${HOST}`);
    const channel = await connection.createChannel();

    const commonOptions = {
        durable: false
    };

    await channel.assertExchange(EXCHANGE, EXCHANGE_TYPE, commonOptions);
    await channel.assertQueue(QUEUE, commonOptions);
    await channel.bindQueue(QUEUE, EXCHANGE, ROUTING_KEY, commonOptions);
    await channel.close();

    return connection;
};

const runSubscriber = async () => {
    const connection = await runAmqp();
    const channel = await connection.createChannel();

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", QUEUE);

    channel.consume(QUEUE, (msg) => {
        const content = JSON.parse(msg.content.toString());

        try {
            const transmisiones = content.map((trx) =>
                [
                    trx.latitud || null,
                    trx.longitud || null,
                    trx.velocidad || null,
                    trx.orientacion || null,
                    trx.fecha_emv || null,
                    trx.id_emv || 0,
                    trx.placa_vehiculo || null,
                    trx.id_evento || 0
                ]
            );

            pg.query(format("select giz.fi_transmision_v2(array[%L]::giz.transmisiones[])", transmisiones));
            console.log(` [x] Received: '${msg.content}' at ${(new Date).toISOString().slice(11, 23).replace('T', ' ')}`);
            channel.ack(msg);
        } catch (error) {
            console.error(`Internal Error: ${error}`);
        }
    });
};

runSubscriber();