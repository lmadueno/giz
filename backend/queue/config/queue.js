const env = process.env;

const config = {
    host: env.QUEUE_HOST || 'localhost',
    exchange: env.QUEUE_EXCHANGE || 'giz',
    type: env.QUEUE_EXCHANGE_TYPE || 'direct',
    name: env.QUEUE_NAME || 'giz_transmision',
    routingKey: env.QUEUE_ROUTING_KEY || 'giz_transmision_key',
};

module.exports = config;