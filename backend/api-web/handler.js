require('dotenv').config();

const express = require('express');
const app = express();
const cors = require('cors');
/*const auth = require("./middleware/auth");
const users = require("./api_modules/users");*/
const seguridad = require("./api_modules/seguridad");
const parametros = require("./api_modules/parametros");
const reportes = require("./api_modules/reportes");
const consultas = require("./api_modules/consultas");
const alertas = require("./api_modules/alertas");
const eett = require("./api_modules/eett");
const rutas = require("./api_modules/rutas");
const emv = require("./api_modules/emv");
const flota = require("./api_modules/flota");
const paraderos = require("./api_modules/paraderos");
const nEmpresas = require("./api_modules/nEmpresas");
const nRutas = require("./api_modules/nRutas");
const nVehiculos = require("./api_modules/nVehiculos");
const cnEmpresas = require("./api_modules/cnEmpresas");
const cnRutas = require("./api_modules/cnRutas");
const cnVehiculos = require("./api_modules/cnVehiculos");
const hexagonos = require("./api_modules/hexagonos");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get('/', (req, res) => { res.send('REST API para plataforma web') });
app.post('/api/seguridad/iniciar-sesion', seguridad.login);
app.get('/api/reportes', /*auth.verifyToken,*/ reportes.getAll);

app.get('/api/reportes/:reporteId/empresas', nEmpresas.getAll);
app.get('/api/reportes/:reporteId/empresas/:empresaId/rutas', nRutas.getAll);
app.get('/api/reportes/:reporteId/empresas/:empresaId/rutas/:rutaId/vehiculos', nVehiculos.getAll);

app.get('/api/consultas', /*auth.verifyToken,*/ consultas.getAll);

app.get('/api/consultas/:consultaId/empresas', cnEmpresas.getAll);
app.get('/api/consultas/:consultaId/empresas/:empresaId/rutas', cnRutas.getAll);
app.get('/api/consultas/:consultaId/empresas/:empresaId/rutas/:rutaId/vehiculos', cnVehiculos.getAll);

app.get('/api/parametros', parametros.getAll);
app.post('/api/parametros', parametros.post);
app.put('/api/parametros/:parametroId', parametros.put);

app.get('/api/alertas', alertas.getAll);
app.post('/api/alertas/:alertaId', alertas.get);
app.post('/api/alertas', alertas.post);
app.put('/api/alertas/:alertaId', alertas.put);

app.get('/api/seguridad', seguridad.getAll);
app.post('/api/seguridad', seguridad.post);
app.put('/api/seguridad/:personaId', seguridad.put);

app.get('/api/eett', eett.getAll);
app.post('/api/eett', eett.post);
app.put('/api/eett/:ettId', eett.put);

app.get('/api/rutas', rutas.getAll);
app.post('/api/rutas', rutas.post);
app.put('/api/rutas/:rutasId', rutas.put);

app.get('/api/emv', emv.getAll);
app.post('/api/emv', emv.post);
app.put('/api/emv/:emvId', emv.put);

app.get('/api/flota/:flotaId', flota.getAll);
app.post('/api/flota/:flotaId', flota.post);
app.put('/api/flota/:placaId', flota.put);
app.put('/api/flota/:flotaId/:placaId', flota.putRuta);

app.get('/api/paraderos', paraderos.getAll);

app.get('/api/hexagonos', hexagonos.getAll);

const port = process.env.APP_PORT;

app.listen(port)
console.log('app running on port', port);