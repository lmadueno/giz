const env = process.env;

const config = {
    db_pg: {
        host: env.PG_HOST || 'localhost',
        port: env.PG_PORT || '5432',
        user: env.PG_USER || 'postgres',
        password: env.PG_PASSWORD || 'postgres',
        database: env.PG_NAME || 'giz',
    },
};

module.exports = config;