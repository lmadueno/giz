const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de Empresas de transporte
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const eett = await pg.query(`
        select
            *,
            case 
                when id_estado_ett ='1' then 'Activo' 
                when id_estado_ett ='2' then 'Inactivo'
            end estado_ett
        from giz.giz_ett`, []);
        return res.status(200).send(camelcaseKeys(eett));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Inserta una nueva Empresa Transporte
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.post = async (req, res) => {
    try {
        if (!(req.body.rucEtt || req.body.razonsocialEmpresa || req.body.direccion))
            return res.status(404).send({ message: "Los campos Ruc, razón social y dirección son obligatorios" });

        const eett = await pg.query(`insert into giz.giz_ett(id_municipalidad,ruc_ett,razon_social_empresa,direccion,id_estado_ett,telefono,correo) values('300331',$1,$2,$3,1,$4,$5) RETURNING *`,
            [req.body.rucEtt, req.body.razonSocialEmpresa, req.body.direccion, req.body.telefono, req.body.correo]
        );
        return res.status(201).send(camelcaseKeys(
            eett
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Actualiza una Empresa de transporte
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.put = async (req, res) => {
    try {
        if (!req.params.ettId)
            return res.status(404).send({ message: "Debe enviar el id de la empresa a actualizar" });

        if (!(req.body.rucEtt || req.body.razonSocialEmpresa || req.body.direccion))
            return res.status(404).send({ message: "Los campos Ruc, razón social son obligatorios" });

        const eett = await pg.query(`
            update giz.giz_ett
            set 
                ruc_ett = $1, 
                razon_social_empresa = $2, 
                direccion= $3, 
                id_estado_ett= $4,
                telefono=$5,
                correo=$6
            where
                id_ett = $7
            RETURNING *`,
            [req.body.rucEtt, req.body.razonSocialEmpresa, req.body.direccion, req.body.idEstadoEtt, req.body.telefono, req.body.correo, req.params.ettId]
        );

        return res.status(200).send(camelcaseKeys(
            eett[0]
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}