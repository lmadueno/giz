const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de alertas
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const alertas = await pg.query(`
        select
           id_alertas as id_alerta,
           nombre_alerta,
           titulo_alerta,
           descripcion_alerta,
           glosa_alerta,
           accion_alerta
        from giz.giz_alertas`, []);
        return res.status(200).send(camelcaseKeys(alertas));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Detalle de una alerta
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.get = async (req, res) => {
    try {
        if (!req.params.alertaId)
            return res.status(404).send({ message: "Debe enviar el id de la alerta" });

        /*const alerta = await pg.query(`
        select
           id_alertas as id_alerta,
           nombre_alerta,
           titulo_alerta,
           descripcion_alerta,
           glosa_alerta,
           accion_alerta
        from giz.giz_alertas
        where
            id_alertas = $1`, [req.params.alertaId]);*/

        //Obtener de parametros metros, velocidad
        /*const coordenadas = await pg.query(`
        select
            t.id_transmision as id,
            t.latitud, 
            t.longitud,
            ST_Distance(  
            (select geom12 from giz.giz_ruta where codigo_ruta = 'A5'),
            ST_MakePoint(t.latitud, t.longitud)
            ) as distancia,
            velocidad
        from giz.giz_transmision t
        inner join giz.giz_ruta r ON ST_DWithin(ST_MakePoint(t.longitud, t.latitud), r.geom12, 500, true)
        where
            velocidad <= 5
        order by 
            distancia DESC`);*/
        /*const coordenadas = await pg.query(`
            select
                t.id_transmision,
                t.latitud, 
                t.longitud,
                ST_Distance(  
                (select geom12 from giz.giz_ruta where codigo_ruta = 'A5'),
                ST_MakePoint(t.longitud, t.latitud),
                true
                ) as distancia,
                velocidad,
                ST_Distance(v.geom, ST_MakePoint(t.longitud, t.latitud), true)
            from giz.giz_transmision t
            inner join giz.giz_ruta r ON ST_DWithin(ST_MakePoint(t.longitud, t.latitud), r.geom12, 500, true)
            inner join(
                select ST_MakePoint(-71.5426377128444, -16.376652323910964) as geom
            ) as v ON ST_DWithin(v.geom, ST_MakePoint(t.longitud, t.latitud), 200, true)
            where
                velocidad <= 5`);*/

        /*
        select 
            (ST_DumpPoints(r.geom12)).geom as geom 
        from giz.giz_ruta r
        where 
            r.codigo_ruta = 'A5'
        */

        const geom = JSON.stringify(req.body);
        const coordenadas = await pg.query(`
        select
            r.geom,
            count(*),
            ST_AsGeoJSON(ST_Union(ST_MakePoint(t.longitud, t.latitud)))::json as geojson
        from(
            select (ST_DumpPoints(ST_GeomFromGeoJSON('${geom}'))).geom
        ) r
        inner join giz.giz_transmision t on ST_DWithin(ST_MakePoint(t.longitud, t.latitud), r.geom, 500, true)
        where
         t.velocidad <= 10
        group by
            r.geom
        having 
            count(*) > 15`);
        return res.status(200).send(camelcaseKeys(coordenadas));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Inserta una nueva alerta
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.post = async (req, res) => {
    try {
        if (!(req.body.nombreAlerta || req.body.tituloAlerta || req.body.descripcionAlerta || req.body.glosaAlerta || req.body.accionAlerta))
            return res.status(404).send({ message: "Los campos Nombre, Titulo, Descripción, Glosa y Acción son obligatorios" });

        const alerta = await pg.query(`insert into giz.giz_alertas(nombre_alerta, titulo_alerta, descripcion_alerta, glosa_alerta, accion_alerta) values ($1, $2, $3, $4, $5) RETURNING *`,
            [req.body.nombreAlerta, req.body.tituloAlerta, req.body.descripcionAlerta, req.body.glosaAlerta, req.body.accionAlerta]
        );

        return res.status(201).send(camelcaseKeys(
            alerta
        ));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Actualizar una alerta
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.put = async (req, res) => {
    try {
        if (!req.params.alertaId)
            return res.status(404).send({ message: "Debe enviar el id de la alerta a actualizar" });

        if (!(req.body.nombreAlerta || req.body.tituloAlerta || req.body.descripcionAlerta || req.body.glosaAlerta || req.body.accionAlerta))
            return res.status(404).send({ message: "Los campos Nombre, Titulo, Descripcion, Glosa y Accion  son obligatorios" });

        const alerta = await pg.query(`
        update 
            giz.giz_alertas
        set 
            nombre_alerta = $1,
            titulo_alerta = $2,
            descripcion_alerta = $3,
            glosa_alerta = $4,
            accion_alerta = $5
        where
            id_alertas = $6
        RETURNING *`,
            [req.body.nombreAlerta, req.body.tituloAlerta, req.body.descripcionAlerta, req.body.glosaAlerta, req.body.accionAlerta, req.params.alertaId]
        );

        return res.status(200).send(camelcaseKeys(
            alerta[0]
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}