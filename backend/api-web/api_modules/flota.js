const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista Flota de vehiculos de una empresa
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        if (!req.params.flotaId)
            return res.status(404).send({ message: "Debe enviar el id de la Flota de transporte." });

            const flota = await pg.query(`
            select 
                    row_number() over (order by gv.placa_vehiculo asc) id,
                    gf.id_ett,
                    gf.id_ruta,
                    gf.placa_vehiculo,
                    gf.id_flota_vehiculos,
                    gv.fecha_registro,
                    gv.id_estado_vehiculo,
                    case when gv.id_estado_vehiculo ='1' then 'Activo' 
                         when gv.id_estado_vehiculo ='2' then 'Inactivo'
                    end estado_vehiculo,
                    gv.codigo_soat,
                    gv.afabricacion_vehiculo,
                    gv.vencimiento_soat,
                    gv.id_tipo_vehiculo,
                    gv.fecha_registro_vehiculo
            from  giz.giz_flotavehiculos gf
            inner join giz.giz_vehiculo gv
                    on gf.placa_vehiculo = gv.placa_vehiculo
            inner join giz.giz_ett ge
                    on ge.id_ett = gf.id_ett
            where gf.id_ett = $1`, [req.params.flotaId]);     
        return res.status(200).send(camelcaseKeys(flota));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Inserta un nuevo Vehiculo a una flota
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.post = async (req, res) => {
    try {
        if (!(req.params.placaId || req.body.codigoSoat || req.body.afabricacionVehiculo || req.body.vencimientoSoat))
            return res.status(404).send({ message: "Los campos Placa, Código SOAT, año de Fab, Fecha de vencimiento SOAT son obligatorios" });

        const vehiculo = await pg.query(`insert into giz.giz_vehiculo(placa_vehiculo,codigo_soat,afabricacion_vehiculo,vencimiento_soat,id_estado_vehiculo,id_tipo_vehiculo) values($1,$2,$3,$4,$5,$6) RETURNING *`,
            [req.body.placaVehiculo, req.body.codigoSoat,req.body.afabricacionVehiculo, req.body.vencimientoSoat,req.body.idEstadoVehiculo,req.body.idTipoVehiculo]);

        const flotaVehiculos = await pg.query(`insert into giz.giz_flotavehiculos(id_municipalidad,id_ett,placa_vehiculo) values('300331',$1,$2) RETURNING *`,
            [req.params.flotaId,vehiculo[0].placa_vehiculo]);

        return res.status(201).send(camelcaseKeys(
            vehiculo
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Actualiza la ruta asignada a la flota
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.putRuta = async (req, res) => {
    try {
        if (!(req.params.flotaId || req.params.placaId))
            return res.status(404).send({ message: "Los campos Flota y placa Vehiculo son obligatorios" });

        const flotaVehiculo = await pg.query(`
        update 
            giz.giz_flotavehiculos
        set 
            id_ruta = $1 
        where
            id_ett= $2 and
            placa_vehiculo = $3 and 
            id_flota_vehiculos= $4`,
        [req.body.idRuta, req.params.flotaId, req.params.placaId, req.body.idFlotaVehiculos]);

        return res.status(200).send(camelcaseKeys(
            flotaVehiculo
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Actualiza un Vehiculo de una flota
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
 exports.put = async (req, res) => {
    try {
        if (!req.params.placaId)
            return res.status(404).send({ message: "Debe enviar la Placa del Vehiculo a actualizar" });

        if (!(req.params.placaId || req.body.codigoSoat || req.body.afabricacionVehiculo || req.body.vencimientoSoat))
            return res.status(404).send({ message: "Los campos Placa, Código SOAT, año de Fab, Fecha de vencimiento SOAT son obligatorios" });

        const flotaVehiculo = await pg.query(`
            update 
                giz.giz_vehiculo
            set 
                afabricacion_vehiculo = $1, 
                id_estado_vehiculo = $2, 
                codigo_soat = $3, 
                vencimiento_soat = $4,
                id_tipo_vehiculo = $5
            where
                placa_vehiculo = $6
            RETURNING 
                *, 
                case when id_estado_vehiculo ='1' then 'Activo' 
                     when id_estado_vehiculo ='2' then 'Inactivo' 
                end estado_vehiculo`,
            [req.body.afabricacionVehiculo, req.body.idEstadoVehiculo, req.body.codigoSoat, req.body.vencimientoSoat, req.body.idTipoVehiculo,req.params.placaId]
        );

        return res.status(200).send(camelcaseKeys(
            flotaVehiculo[0]
        ));

    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
