const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la consulta solicitada a nivel vehiculos 
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    if (!req.params.consultaId)
        return res.status(404).send({ message: "Debe enviar el parámetro consultaId" });

    if (!req.params.empresaId)
        return res.status(404).send({ message: "Debe enviar el parámetro empresaId" });

    if (!req.params.rutaId)
        return res.status(404).send({ message: "Debe enviar el parámetro rutaId" });

    const consultaId = parseInt(req.params.consultaId);

    if (consultaId === 1) return consulta1(req, res);
    if (consultaId === 3) return consulta3(req, res);
    if (consultaId === 4) return consulta4(req, res);
    if (consultaId === 5) return consulta5(req, res);
    if (consultaId === 10) return consulta10(req, res);
    if (consultaId === 11) return consulta11(req, res);
    if (consultaId === 16) return consulta16(req, res);
    if (consultaId === 19) return consulta19(req, res);
    if (consultaId === 21) return consulta21(req, res);
    if (consultaId === 24) return consulta24(req, res);
   
}

const consulta1 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos,
            round((random() * 10 + 1)::numeric, 2)  as porc_total
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'Promedio de Antigüedad de Vehículos en Servicio de las Empresas de Transporte del Sistema (Años)' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                     select 
                     case when y=1	
                         then 'Kilometros Recorridos' 
                         else 'Porcentaje Total' end as name, 
                     case when y=1 
                         then 'column'  
                         else 'spline'  end as type, 
                     case when y=1
                         then 1
                         else 0 end as \"yAxis\",
                     case when y=1
                         then JSON_AGG(x.km_recorridos) 
                         else JSON_AGG(x.porc_total) end as data
                     from  
                     generate_series(1,2)y 
                   )c
                )
            from(
                select 
                    id,
                    'AVX' || '-' || (random() * 500 + 100)::int as placa,
                    floor(random() * 10 + 1)::int as km_recorridos,
                    round((random() * 10 + 1)::numeric, 2)  as porc_total
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta3 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as pro_antiguedad_servicio
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'column' as type) t) as chart,
                (select row_to_json(t) from(select 'Promedio de Antigüedad de Vehículos en Servicio de las Empresas de Transporte del Sistema (Años)' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de Años' as text) t) t) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                select
                    array_to_json(array_agg(x)) as series
                from(      
                select 'Vehiculos' as name,JSON_AGG(x.pro_antiguedad_servicio) as data
                ) x
                )
            from(
                select 
                    id,
                    'AVX' || '-' || (random() * 500 + 100)::int as placa,
                    floor(random() * 10 + 1)::int as pro_antiguedad_servicio
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta4 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta5 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta10 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta11 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta16 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta19 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta21 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const consulta24 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            floor(random() * 10 + 1)::int as km_recorridos_ruta,
            floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
            round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
        from generate_series(1, 10) as id`, []);
        
        const chart = await pg.query(`
        select
            row_to_json(x) as options
        FROM(
            select
                (select row_to_json(t) from(select 'KILÓMETROS RECORRIDOS FUERA DE RUTA' as text) t) as title,
                (select row_to_json(t) from(select false as enabled) t) as credits,
                (select row_to_json(t) FROM(select json_agg(x.placa) AS categories) t) as \"xAxis\",
                (select array_to_json(array_agg(to_json(u))) from (select to_json(t) from(select to_json(t)as title from(select case when y=1 then 'Kilometros Recorridos' else 'Porcentaje Total' end as text from generate_series(1,2)y ) t ) t union all select to_json('opposite:'||'true'::text) as opposite )u) as \"yAxis\",
                (select row_to_json(t) from(select true as enabled) t) as exporting,
                (               
                    select 
                    array_to_json(array_agg(to_json(c))) as series
                    from (
                        select 
                        case when y=1 then 'Kilómetros recorridos en Ruta' 
                            when y=2 then 'Kilómetros recorridos fuera de Ruta' 
                            when y=3 then 'Porcentaje de Kilómetros Recorridos fuera de Ruta' end as name, 
                        case 
                            when y=1 then 'column'  
                            when y=2 then 'column'  
                            when y=3 then 'spline'  
                            end as type, 
                        case when y=1
                            then 1
                            else 0 end as \"yAxis\",
                        case 
                            when y=1 then JSON_AGG(x.km_recorridos_ruta) 
                            when y=2 then JSON_AGG(x.km_recorridos_fuera_ruta) 
                            when y=3 then JSON_AGG(x.porc_km_recorridos_fuera_ruta) 
                        end as data
                                from  
                        generate_series(1,3)y 
                    )c
                )
            from(
                select 
                    id,
                    'Placa' || ' ' || id as placa,
                    floor(random() * 10 + 1)::int as km_recorridos_ruta,
                    floor(random() * 10 + 1)::int as km_recorridos_fuera_ruta,
                    round((random() * 10 + 1)::numeric, 2)  as porc_km_recorridos_fuera_ruta
                from generate_series(1, 10) as id
            ) x
        ) x`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            chart: camelcaseKeys(chart[0].options)        
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
