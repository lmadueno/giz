const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de paraderos
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const paraderos = await pg.query(`
        SELECT 
            id_paradero, 
            id_municipalidad, 
            id_ruta, 
            nombre_paradero, 
            glosa_paradero, 
            ST_Y(geom) as latitud, 
            ST_X(geom) as longitud, 
            sentido 
        FROM giz.giz_paradero`, []);
        return res.status(200).send(camelcaseKeys(paraderos));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}