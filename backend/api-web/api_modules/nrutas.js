const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna el reporte solicitado 
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    if (!req.params.reporteId)
        return res.status(404).send({ message: "Debe enviar el parámetro reporteId" });

    if (!req.params.empresaId)
        return res.status(404).send({ message: "Debe enviar el parámetro empresaId" });

    const reporteId = parseInt(req.params.reporteId);

    if (reporteId === 1) return reporte1(req, res);
    if (reporteId === 3) return reporte3(req, res);
    if (reporteId === 4) return reporte4(req, res);
    if (reporteId === 5) return reporte5(req, res);
    if (reporteId === 6) return reporte6(req, res);
    if (reporteId === 7) return reporte7(req, res);
    if (reporteId === 8) return reporte8(req, res);
    if (reporteId === 10) return reporte10(req, res);
    if (reporteId === 11) return reporte11(req, res);
    if (reporteId === 12) return reporte12(req, res);
    if (reporteId === 14) return reporte14(req, res);
    if (reporteId === 15) return reporte15(req, res);
}

const reporte1 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            row_number() over(order by id_ruta) id,
            id_ruta ruta,
            count(placa_vehiculo) buses,
            sum(s_12) salidas_1_2,
            sum(v_in_12) viajes_incompletos_1_2,
            sum(v_c_12) viajes_completos_1_2,
            sum(s_21) salidas_2_1,
            sum(v_in_21) viajes_incompletos_2_1,
            sum(v_c_21) viajes_completos_2_1,	
            round(sum(v_c_12)/sum(s_12),3) porc_viajes_completos_1_2,		
            round(sum(v_c_21)/sum(s_21),3) porc_viajes_completos_2_1		
        from (select 
        --	id_ett,
            id_ruta,
            placa_vehiculo,
            sum(salida_1_2)s_12,
            sum(viajes_incompletos_1_2)v_in_12,
            sum(salida_1_2)-sum(viajes_incompletos_1_2)v_c_12,
            sum(salida_2_1)s_21,
            sum(viajes_incompletos_2_1)v_in_21,
            sum(salida_2_1)-sum(viajes_incompletos_2_1)v_c_21	
            from giz_resumen.giz_reporte01 
        --	where fecha='2021-10-05' coloca los filtros
            group by 
        --	id_ett,
            id_ruta,
            placa_vehiculo,
            salida_1_2,
            viajes_incompletos_1_2,
            salida_2_1,
            viajes_incompletos_2_1 
            order by 1,2)x 
        group by 
        --placa_vehiculo,
        id_ruta order by 1,2`, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '% de Viajes completos en Sentido 1-2 en todas las Rutas de la EETT:'
                when id = 2 then '% de Viajes completos en Sentido 2-1 en todas las Rutas de la EETT:'
                when id = 3 then '% de Viajes completos en ambos Sentidos en todas las Rutas de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);
    
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte3 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as rutas,
            floor(random() * 200 + 1)::int as buses_autorizados,
            floor(random() * 2000 + 1)::int as buses_en_serv_1_2,
            floor(random() * 2000 + 1)::int as buses_en_serv_2_1,
            round((random() * 10 + 1)::numeric, 2) porc_transmision_1_2,
            round((random() * 10 + 1)::numeric, 2) porc_transmision_2_1
        from generate_series(1, 10) as id`, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '% de Transmisión del GPS por minuto Sentido 1-2 en todas las Rutas de la EETT:'
                when id = 2 then '% de Transmisión del GPS por minuto Sentido 2-1 en todas las Rutas de la EETT:'
                when id = 3 then '% de Transmisión del GPS por minuto en ambos Sentidos en todas las Rutas de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);
      
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte4 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as longitud,
            floor(random() * 200 + 1)::int as n_paradas_ruta,
            floor(random() * 2000 + 1)::int as n_viajes_completo,
            floor(random() * 2000 + 1)::int as v_media_ruta12,
            floor(random() * 2000 + 1)::int as v_media_ruta21
        from generate_series(1, 10) as id
        `, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Velocidad Media en Sentido 1-2 de todas las Rutas de la EETT:'
                when id = 2 then 'Velocidad Media en Sentido 2-1 de todas las Rutas de la EETT:'
                when id = 3 then 'Velocidad Media en ambos  Sentidos de todas las Rutas de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);
        
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}


const reporte5 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as v_autorizados,
            floor(random() * 200 + 1)::int as v_con_GPS,
            floor(random() * 2000 + 1)::int as v_sin_GPS,
            floor(random() * 2000 + 1)::int as v_Servicio_C_GPS,
            floor(random() * 2000 + 1)::int as p_Vehiculos_Servicio_C_GPS
        from generate_series(1, 10) as id`, []);
        const resumen = await pg.query(`
            select 
                id,
                case
                    when id = 1 then '% de Vehículos en Servicio con GPS en la EETT'
                end as concepto,
                round((random() * 10 + 1)::numeric, 2) porcentaje
            from generate_series(1, 1) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen),
        });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte6 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as v_autorizados,
            floor(random() * 200 + 1)::int as v_con_gps,
            floor(random() * 2000 + 1)::int as a_boton_panico,
            floor(random() * 2000 + 1)::int as n_vehiculos_act_btn,
            floor(random() * 2000 + 1)::int as p_vehiculos_servicio_gps
            from generate_series(1, 10) as id   `, []);

        const resumen = await pg.query(`
            select 
                id,
                case
                    when id = 1 then '% de Alertas de Pánico por Vehículos con GPS en las Rutas de la EETT:'
                end as concepto,
                round((random() * 10 + 1)::numeric, 2) porcentaje
            from generate_series(1, 1) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte7 = async (req, res) => {
    try {
        const sentido_1_2 = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as v_autorizados,
            floor(random() * 200 + 1)::int as longitud_rutas,
            floor(random() * 2000 + 1)::int as n_completos_ett,
            floor(random() * 2000 + 1)::int as n_incompletos_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_con_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_inc_ett,
            floor(random() * 2000 + 1)::int as tot_km_recorridos_s12,
            floor(random() * 2000 + 1)::int as porc_km_recorridos_s12
        from generate_series(1, 10) as id  `, []);
        
        const sentido_2_1 = await pg.query(`
        select 
            id,
                'Ruta' || ' ' || id as ruta,
                floor(random() * 10 + 1)::int as v_autorizados,
                floor(random() * 200 + 1)::int as longitud_rutas,
                floor(random() * 2000 + 1)::int as n_completos_ett,
                floor(random() * 2000 + 1)::int as n_incompletos_ett,
                floor(random() * 2000 + 1)::int as km_recorridos_con_ett,
                floor(random() * 2000 + 1)::int as km_recorridos_inc_ett,
                floor(random() * 2000 + 1)::int as tot_km_recorridos_s12,
                floor(random() * 2000 + 1)::int as porc_km_recorridos_s12
        from generate_series(1, 10) as id  `, []);
        
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Kilómetros recorridos en las Rutas de la EETT en el Sentido 1-2'
                when id = 2 then 'Kilómetros recorridos en las Rutas de la EETT en el Sentido 2-1'
                when id = 3 then 'Kilómetros recorridos en las Rutas de la EETT en ambos Sentidos'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);

        return res.status(200).send({
            sentido_1_2:camelcaseKeys(sentido_1_2),
            sentido_2_1:camelcaseKeys(sentido_2_1),
            resumen:camelcaseKeys(resumen),
        });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte8 = async (req, res) => {
    try {
        const sentido_1_2 = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as v_autorizados,
            floor(random() * 200 + 1)::int as longitud_rutas,
            floor(random() * 2000 + 1)::int as n_paraderos_ruta,
            floor(random() * 2000 + 1)::int as n_viajes_detenciones_100,
            floor(random() * 2000 + 1)::int as n_viajes_detenciones_inc,
            floor(random() * 2000 + 1)::int as n_detenciones_no_realizadas_paraderos,
            floor(random() * 2000 + 1)::int as porc_viajes_detenciones_paraderos_12
        from generate_series(1, 10) as id `, []);
        const sentido_2_1 = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 10 + 1)::int as v_autorizados,
            floor(random() * 200 + 1)::int as longitud_rutas,
            floor(random() * 2000 + 1)::int as n_paraderos_ruta,
            floor(random() * 2000 + 1)::int as n_viajes_detenciones_100,
            floor(random() * 2000 + 1)::int as n_viajes_detenciones_inc,
            floor(random() * 2000 + 1)::int as n_detenciones_no_realizadas_paraderos,
            floor(random() * 2000 + 1)::int as porc_viajes_detenciones_paraderos_12
        from generate_series(1, 10) as id  `, []);
       
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '%  de Viajes con Detenciones de Vehículos en Paraderos en Sentido 1-2 en todas las Rutas de la EETT:'
                when id = 2 then '%  de Viajes con Detenciones de Vehículos en Paraderos en Sentido 2-1 en todas las Rutas de la EETT:'
                when id = 3 then '%  de Viajes con Detenciones de Vehículos en Paraderos en ambos Sentido en todas las Rutas de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);
        return res.status(200).send({
            sentido_1_2:camelcaseKeys(sentido_1_2),
            sentido_2_1:camelcaseKeys(sentido_2_1),
            resumen:camelcaseKeys(resumen),
        });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte10 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 2000 + 1)::int as km_recorridos_gps,            
            floor(random() * 2000 + 1)::int as km_recorridos_ruta,            
            floor(random() * 2000 + 1)::int as porc_km_recorridos_ruta_vs_gps 
            from generate_series(1, 10) as id   `, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '%  de Km Recorridos en Rutas de la EETT (Rutas vs. GPS)'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 1) as id`, []);

    return res.status(200).send({
        detalle:camelcaseKeys(detalle),
        resumen:camelcaseKeys(resumen),
    });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte11 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 2000 + 1)::int as tot_km_recorridos,            
            floor(random() * 2000 + 1)::int as pago_subsidio         
            from generate_series(1, 10) as id`, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Pago de Subsidio por Rutas, según kilómetros recorridos '
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 1) as id`, []);
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte12 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 2000 + 1)::int as km_recorridos_fuera_ruta            
            from generate_series(1, 10) as id`, []);
        return res.status(200).send({
            detalle:camelcaseKeys(detalle)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte14 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
        id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 2000 + 1)::int as tiempo_sin_transmision_gps            
            from generate_series(1, 10) as id`, []);
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
        });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte15 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'Ruta' || ' ' || id as ruta,
            floor(random() * 2000 + 1)::int as tot_tiempo_acumulado_o_s            
            from generate_series(1, 10) as id   `, []);
        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
    });
       
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

