const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de reportes
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const reportes = await pg.query(`
        select
            id,
            case
                when id = 1 then 'Cumplimiento del Plan de Rutas.'
                when id = 2 then 'Reporte de tiempo de unidades con motor apagado.'
                when id = 3 then 'Estado de Transmisión del GPS.'
                when id = 4 then 'Velocidad Media durante la prestación del Servicio.'
                when id = 5 then 'Vehículos con GPS por Ruta y EETT.'
                when id = 6 then 'Alertas del Botón de Pánico en los Vehículos.'
                when id = 7 then 'Historial del recorrido (km) de Vehículos en Ruta.'
                when id = 8 then 'Cumplimiento de Detención de Vehículos en Paraderos.'
                when id = 9 then 'Indicadores de Empresas de Transporte Registradas.'
                when id = 10 then 'Comparativo de Kilómetros Recorridos según Ruta y Kilómetros Recorridos segùn GPS.'
                when id = 11 then 'Vehículos que cumplen con los kilòmetros vàlidos para Subsidio.'
                when id = 12 then 'Kilómetros recorridos por Vehículos fuera de la Ruta y en horario del Servicio.'
                when id = 13 then 'Reporte de apagado y encendido de motor dentro de la ruta, tramo, geocerca.'
                when id = 14 then 'Tiempo sin transmisión de GPS por Vehículos durante la prestación del Servicio .'
                when id = 15 then 'Tiempo Acumulado por Vehículos en la Operación del Servicio .'
            end as name
        from generate_series(1, 15) as id`, []);
        return res.status(200).send(camelcaseKeys(reportes));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}