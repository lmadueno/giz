const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna el reporte solicitado 
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    if (!req.params.reporteId)
        return res.status(404).send({ message: "Debe enviar el parámetro reporteId" });

    if (!req.params.empresaId)
        return res.status(404).send({ message: "Debe enviar el parámetro empresaId" });

    if (!req.params.rutaId)
        return res.status(404).send({ message: "Debe enviar el parámetro rutaId" });

    const reporteId = parseInt(req.params.reporteId);

    if (reporteId === 1) return reporte1(req, res);
    if (reporteId === 3) return reporte3(req, res);
    if (reporteId === 4) return reporte4(req, res);
    if (reporteId === 7) return reporte7(req, res);
    if (reporteId === 10) return reporte10(req, res);
    if (reporteId === 11) return reporte11(req, res);
    if (reporteId === 12) return reporte12(req, res);
    if (reporteId === 14) return reporte14(req, res);
    if (reporteId === 15) return reporte15(req, res);
}

const reporte1 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select placa_vehiculo placa,
            sum(s_12) salidas_1_2,
            sum(v_in_12)viajes_incompletos_1_2,
            sum(v_c_12)viajes_completos_1_2,
            sum(s_21)salidas_2_1,
            sum(v_in_21)viajes_incompletos_2_1,
            sum(v_c_21)viajes_completos_2_1,	
            round(sum(v_c_12)/sum(s_12),3)porc_viajes_completos_1_2,		
            round(sum(v_c_21)/sum(s_21),3)porc_viajes_completos_2_1		
        from (select 
        --	id_ett,
        --	id_ruta,
            placa_vehiculo,
            sum(salida_1_2)s_12,
            sum(viajes_incompletos_1_2)v_in_12,
            sum(salida_1_2)-sum(viajes_incompletos_1_2)v_c_12,
            sum(salida_2_1)s_21,
            sum(viajes_incompletos_2_1)v_in_21,
            sum(salida_2_1)-sum(viajes_incompletos_2_1)v_c_21	
            from giz_resumen.giz_reporte01 
        --	where fecha='2021-10-05' coloca los filtros
            group by 
        --	id_ett,
        --	id_ruta,
            placa_vehiculo,
            salida_1_2,
            viajes_incompletos_1_2,
            salida_2_1,
            viajes_incompletos_2_1 
            order by 1,2)x 
        group by placa_vehiculo`, []);
        
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '% de Viajes completos en Sentido 1-2 en la Ruta (Código) de la EETT:'
                when id = 2 then '% de Viajes completos en Sentido 2-1 en la Ruta (Código) de la EETT:'
                when id = 3 then '% de Viajes completos en ambos Sentidos en la Ruta (Código) de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);

        return res.status(200).send({
            detalle: camelcaseKeys(detalle),
            resumen: camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte3 = async (req, res) => {
    try {
        const sentido_1_2 = await pg.query(`
        select 
            row_number() over(order by a.placa_vehiculo) id,
            a.placa_vehiculo placa,
            coalesce(b.porc_transmision_1_2*100,'0')|| '%' h1,
            coalesce(c.porc_transmision_1_2*100,'0')|| '%'  h2,
            coalesce(d.porc_transmision_1_2*100,'0')|| '%'  h3,
            coalesce(e.porc_transmision_1_2*100,'0')|| '%'  h4,
            coalesce(f.porc_transmision_1_2*100,'0')|| '%'  h5,
            coalesce(g.porc_transmision_1_2*100,'0')|| '%'  h6,
            coalesce(h.porc_transmision_1_2*100,'0')|| '%'  h7,
            coalesce(i.porc_transmision_1_2*100,'0')|| '%'  h8,
            coalesce(j.porc_transmision_1_2*100,'0')|| '%'  h9,
            coalesce(k.porc_transmision_1_2*100,'0')|| '%'  h10,
            coalesce(l.porc_transmision_1_2*100,'0')|| '%'  h11,
            coalesce(m.porc_transmision_1_2*100,'0')|| '%'  h12,
            coalesce(n.porc_transmision_1_2*100,'0')|| '%'  h13,
            coalesce(o.porc_transmision_1_2*100,'0')|| '%'  h14,
            coalesce(p.porc_transmision_1_2*100,'0')|| '%'  h15,
            coalesce(q.porc_transmision_1_2*100,'0')|| '%'  h16,
            coalesce(r.porc_transmision_1_2*100,'0')|| '%'  h17,
            coalesce(s.porc_transmision_1_2*100,'0')|| '%'  h18,
            coalesce(t.porc_transmision_1_2*100,'0')|| '%'  h19,
            coalesce(u.porc_transmision_1_2*100,'0')|| '%'  h20,
            coalesce(v.porc_transmision_1_2*100,'0')|| '%'  h21,
            coalesce(x.porc_transmision_1_2*100,'0')|| '%'  h22,
            coalesce(y.porc_transmision_1_2*100,'0')|| '%'  h23,
            coalesce(z.porc_transmision_1_2*100,'0')|| '%'  h24,
            round((select avg(porc_transmision_1_2*100) from giz_resumen.giz_reporte03 where placa_vehiculo=a.placa_vehiculo),3)||'%' porc_sentido_12
        from
            (select placa_vehiculo from giz_resumen.giz_reporte03  group by placa_vehiculo)a left join 
            (select * from giz_resumen.giz_reporte03 where hora=1)b on a.placa_vehiculo=b.placa_vehiculo left join 
            (select * from giz_resumen.giz_reporte03 where hora=2)c on a.placa_vehiculo=c.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=3)d on a.placa_vehiculo=d.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=4)e on a.placa_vehiculo=e.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=5)f on a.placa_vehiculo=f.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=6)g on a.placa_vehiculo=g.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=7)h on a.placa_vehiculo=h.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=8)i on a.placa_vehiculo=i.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=9)j on a.placa_vehiculo=j.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=10)k on a.placa_vehiculo=k.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=11)l on a.placa_vehiculo=l.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=12)m on a.placa_vehiculo=m.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=13)n on a.placa_vehiculo=n.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=14)o on a.placa_vehiculo=o.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=15)p on a.placa_vehiculo=p.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=16)q on a.placa_vehiculo=q.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=17)r on a.placa_vehiculo=r.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=18)s on a.placa_vehiculo=s.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=19)t on a.placa_vehiculo=t.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=20)u on a.placa_vehiculo=u.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=21)v on a.placa_vehiculo=v.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=22)x on a.placa_vehiculo=x.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=23)y on a.placa_vehiculo=y.placa_vehiculo left join
            (select * from giz_resumen.giz_reporte03 where hora=24)z on a.placa_vehiculo=z.placa_vehiculo `, []);

        const sentido_2_1 = await pg.query(`
        select
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            round((random() * 10 + 1)::numeric, 2) as h5,
            round((random() * 10 + 1)::numeric, 2) as h6,
            round((random() * 10 + 1)::numeric, 2) as h7,
            round((random() * 10 + 1)::numeric, 2) as h8,
            round((random() * 10 + 1)::numeric, 2) as h9,
            round((random() * 10 + 1)::numeric, 2) as h10,
            round((random() * 10 + 1)::numeric, 2) as h11,
            round((random() * 10 + 1)::numeric, 2) as h12,
            round((random() * 10 + 1)::numeric, 2) as h13,
            round((random() * 10 + 1)::numeric, 2) as h14,
            round((random() * 10 + 1)::numeric, 2) as h15,
            round((random() * 10 + 1)::numeric, 2) as h16,
            round((random() * 10 + 1)::numeric, 2) as h17,
            round((random() * 10 + 1)::numeric, 2) as h18,
            round((random() * 10 + 1)::numeric, 2) as h19,
            round((random() * 10 + 1)::numeric, 2) as h20,
            round((random() * 10 + 1)::numeric, 2) as h21,
            round((random() * 10 + 1)::numeric, 2) as h22,
            round((random() * 10 + 1)::numeric, 2) as h23,
            round((random() * 10 + 1)::numeric, 2) porc_transmision_1_2
        from generate_series(1, 5) as id`, []);

        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '% de Transmisión del GPS por minuto Sentido 1-2 en la Ruta (Código) de la EETT:'
                when id = 2 then '% de Transmisión del GPS por minuto Sentido 2-1 en la Ruta (Código) de la EETT:'
                when id = 3 then '% de Transmisión del GPS por minuto en ambos Sentidos en la Ruta (Código) de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);

        return res.status(200).send({
            sentido12: camelcaseKeys(sentido_1_2),
            sentido21: camelcaseKeys(sentido_2_1),
            resumen: camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte4 = async (req, res) => {
    try {
        const sentido_1_2 = await pg.query(`
        select
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            round((random() * 10 + 1)::numeric, 2) as h5,
            round((random() * 10 + 1)::numeric, 2) as h6,
            round((random() * 10 + 1)::numeric, 2) as h7,
            round((random() * 10 + 1)::numeric, 2) as h8,
            round((random() * 10 + 1)::numeric, 2) as h9,
            round((random() * 10 + 1)::numeric, 2) as h10,
            round((random() * 10 + 1)::numeric, 2) as h11,
            round((random() * 10 + 1)::numeric, 2) as h12,
            round((random() * 10 + 1)::numeric, 2) as h13,
            round((random() * 10 + 1)::numeric, 2) as h14,
            round((random() * 10 + 1)::numeric, 2) as h15,
            round((random() * 10 + 1)::numeric, 2) as h16,
            round((random() * 10 + 1)::numeric, 2) as h17,
            round((random() * 10 + 1)::numeric, 2) as h18,
            round((random() * 10 + 1)::numeric, 2) as h19,
            round((random() * 10 + 1)::numeric, 2) as h20,
            round((random() * 10 + 1)::numeric, 2) as h21,
            round((random() * 10 + 1)::numeric, 2) as h22,
            round((random() * 10 + 1)::numeric, 2) as h23,
            round((random() * 10 + 1)::numeric, 2)  v_media_kmh
        from generate_series(1, 5) as id`, []);

        const sentido_2_1 = await pg.query(`
        select
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,
            round((random() * 10 + 1)::numeric, 2) as h5,
            round((random() * 10 + 1)::numeric, 2) as h6,
            round((random() * 10 + 1)::numeric, 2) as h7,
            round((random() * 10 + 1)::numeric, 2) as h8,
            round((random() * 10 + 1)::numeric, 2) as h9,
            round((random() * 10 + 1)::numeric, 2) as h10,
            round((random() * 10 + 1)::numeric, 2) as h11,
            round((random() * 10 + 1)::numeric, 2) as h12,
            round((random() * 10 + 1)::numeric, 2) as h13,
            round((random() * 10 + 1)::numeric, 2) as h14,
            round((random() * 10 + 1)::numeric, 2) as h15,
            round((random() * 10 + 1)::numeric, 2) as h16,
            round((random() * 10 + 1)::numeric, 2) as h17,
            round((random() * 10 + 1)::numeric, 2) as h18,
            round((random() * 10 + 1)::numeric, 2) as h19,
            round((random() * 10 + 1)::numeric, 2) as h20,
            round((random() * 10 + 1)::numeric, 2) as h21,
            round((random() * 10 + 1)::numeric, 2) as h22,
            round((random() * 10 + 1)::numeric, 2) as h23,
            round((random() * 10 + 1)::numeric, 2) v_media_kmh
        from generate_series(1, 5) as id`, []);

        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Velocidad Media en Sentido 1-2 en la Ruta (Código) de la EETT:'
                when id = 2 then 'Velocidad Media en Sentido 2-1 en la Ruta (Código) de la EETT:'
                when id = 3 then 'Velocidad Media en ambos Sentidos en la Ruta (Código) de la EETT:'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);

        return res.status(200).send({
            sentido12: camelcaseKeys(sentido_1_2),
            sentido21: camelcaseKeys(sentido_2_1),
            resumen: camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte7 = async (req, res) => {
    try {
        const sentido_1_2 = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,           
            floor(random() * 2000 + 1)::int as n_completos_ett,
            floor(random() * 2000 + 1)::int as n_incompletos_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_con_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_inc_ett,
            floor(random() * 2000 + 1)::int as tot_km_recorridos_s12,
            floor(random() * 2000 + 1)::int as porc_km_recorridos_s12
        from generate_series(1, 10) as id  `, []);

        const sentido_2_1 = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,           
            floor(random() * 2000 + 1)::int as n_completos_ett,
            floor(random() * 2000 + 1)::int as n_incompletos_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_con_ett,
            floor(random() * 2000 + 1)::int as km_recorridos_inc_ett,
            floor(random() * 2000 + 1)::int as tot_km_recorridos_s12,
            floor(random() * 2000 + 1)::int as porc_km_recorridos_s12
        from generate_series(1, 10) as id  `, []);

        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Kilómetros recorridos en la Rutas(codigo) de la EETT en el Sentido 1-2'
                when id = 2 then 'Kilómetros recorridos en la Rutas(codigo) de la EETT en el Sentido 2-1'
                when id = 3 then 'Kilómetros recorridos en las Rutas de la EETT en ambos Sentidos'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 3) as id`, []);

        return res.status(200).send({
            sentido12: camelcaseKeys(sentido_1_2),
            sentido21: camelcaseKeys(sentido_2_1),
            resumen: camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte10 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
             id,
             'AVX' || '-' || (random() * 500 + 100)::int as placa,       
            floor(random() * 2000 + 1)::int as km_recorridos_gps,            
            floor(random() * 2000 + 1)::int as km_recorridos_ruta,            
            floor(random() * 2000 + 1)::int as porc_km_recorridos_ruta_vs_gps 
            from generate_series(1, 10) as id `, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then '%  de Km Recorridos por los Vehìculos (Rutas vs. GPS)'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 1) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

const reporte11 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
        id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,       
            (select array_to_string(array(select substr('ABCD', trunc(random() * 4)::integer + 1, 1) from generate_series(1, 1)), '')) as tipo_vehiculo,       
            floor(random() * 2000 + 1)::int as subsidio_km,            
            floor(random() * 2000 + 1)::int as tot_km_recorridos,            
            floor(random() * 2000 + 1)::int as pago_subsidio 
        from generate_series(1, 10) as id `, []);
        const resumen = await pg.query(`
        select 
            id,
            case
                when id = 1 then 'Pago de Subsidio por Vehículos, según kilómetros recorridos en su Ruta'
            end as concepto,
            round((random() * 10 + 1)::numeric, 2) porcentaje
        from generate_series(1, 1) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle),
            resumen:camelcaseKeys(resumen),
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte12 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,       
            floor(random() * 2000 + 1)::int as km_recorridos_fuera_ruta            
        from generate_series(1, 10) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte14 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,       
            floor(random() * 2000 + 1)::int as tiempo_sin_transmision_gps            
            from generate_series(1, 10) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
const reporte15 = async (req, res) => {
    try {
        const detalle = await pg.query(`
        select 
            id,
            'AVX' || '-' || (random() * 500 + 100)::int as placa,       
            floor(random() * 2000 + 1)::int as tot_tiempo_acumulado_o_s            
        from generate_series(1, 10) as id`, []);

        return res.status(200).send({
            detalle:camelcaseKeys(detalle)
        });
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}
