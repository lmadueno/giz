const pg = require('../services/pg');
const pgFormat = require('pg-format');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de rutas con Objetos
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const rutas = await pg.query(`
        select
            id_municipalidad,
            id_ruta,
            id_patio,
            codigo_ruta,
            nombre_ruta,
            glosa_ruta,
            detalle_ruta,
            sentido,
            ST_AsGeoJSON(geom12)::json as geom12,
            ST_AsGeoJSON(geom21)::json as geom21
        from giz.giz_ruta
        where
            geom12 is not null or
            geom21 is not null`, []);

        return res.status(200).send(camelcaseKeys(rutas));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Inserta una nueva ruta
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.post = async (req, res) => {
    try {
        if (!req.body.codigoRuta)
            return res.status(404).send({ message: "Debe enviar el código de la ruta" });

        if (!req.body.nombreRuta)
            return res.status(404).send({ message: "Debe enviar el nombre de la ruta" });

        await pg.query(`create temporary table if not exists temp_rutas_geom(geom text, sentido smallint)`, []);

        if (req.body.geom12 && req.body.geom12.features)
            await pg.query(pgFormat('insert into temp_rutas_geom(geom, sentido) values %L', getFeatures(req.body.geom12, 1)), []);

        if (req.body.geom21 && req.body.geom21.features)
            await pg.query(pgFormat('insert into temp_rutas_geom(geom, sentido) values %L', getFeatures(req.body.geom21, 2)), []);

        const rutas = await pg.query(`
        insert into giz.giz_ruta(id_municipalidad, codigo_ruta, nombre_ruta, geom12, geom21) 
        values(
            $1, 
            $2, 
            $3, 
            (select ST_Union(ST_GeomFromGeoJSON(geom)) from temp_rutas_geom where sentido = 1),
            (select ST_Union(ST_GeomFromGeoJSON(geom)) from temp_rutas_geom where sentido = 2)
        ) RETURNING *`, ['300331', req.body.codigoRuta, req.body.nombreRuta]);

        await pg.query(`drop table if exists temp_rutas_geom`, []);

        return res.status(201).send(camelcaseKeys(
            rutas.map(ruta => ({
                ...ruta,
                geom12: req.body.geom12 ? JSON.stringify(req.body.geom12) : '',
                geom21: req.body.geom21 ? JSON.stringify(req.body.geom21) : ''
            }))
        ));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Actualiza una ruta
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.put = async (req, res) => {
    try {
        if (!req.body.codigoRuta)
            return res.status(404).send({ message: "Debe enviar el código de la ruta" });

        if (!req.body.nombreRuta)
            return res.status(404).send({ message: "Debe enviar el nombre de la ruta" });

        await pg.query(`create temporary table if not exists temp_rutas_geom(geom text, sentido smallint)`, []);

        if (req.body.geom12 && req.body.geom12.features)
            await pg.query(pgFormat('insert into temp_rutas_geom(geom, sentido) values %L', getFeatures(req.body.geom12, 1)), []);

        if (req.body.geom21 && req.body.geom21.features)
            await pg.query(pgFormat('insert into temp_rutas_geom(geom, sentido) values %L', getFeatures(req.body.geom21, 2)), []);

        const rutas = await pg.query(`
            update
                giz.giz_ruta
            set
                codigo_ruta = $1,
                nombre_ruta = $2,
                geom12 = (select ST_Union(ST_GeomFromGeoJSON(geom)) from temp_rutas_geom where sentido = 1),
                geom21 = (select ST_Union(ST_GeomFromGeoJSON(geom)) from temp_rutas_geom where sentido = 2)
            where
                id_ruta = $3
            RETURNING *`, [req.body.codigoRuta, req.body.nombreRuta, req.body.idRuta]);

        await pg.query(`drop table if exists temp_rutas_geom`, []);

        return res.status(200).send(camelcaseKeys(
            rutas.map(ruta => ({
                ...ruta,
                geom12: req.body.geom12 ? JSON.stringify(req.body.geom12) : '',
                geom21: req.body.geom21 ? JSON.stringify(req.body.geom21) : ''
            }))[0]
        ));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}

/**
 * Retorna un arreglo con las geometrías del GeoJSON
 * @param {GeoJSON} geoJSON 
 * @param {Number} sentido 
 * @returns 
 */
const getFeatures = (geoJSON, sentido = 1) => {
    let geometry = [];

    if (geoJSON.features)
        geoJSON.features.forEach(feature => {
            geometry.push([
                JSON.stringify(feature.geometry),
                sentido
            ]);
        })

    return geometry;
}