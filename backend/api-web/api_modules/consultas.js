const pg = require('../services/pg');
const camelcaseKeys = require('camelcase-keys');

/**
 * Retorna la lista de consultas
 * @param {Object} req 
 * @param {Object} res 
 * @returns 
 */
exports.getAll = async (req, res) => {
    try {
        const consulta = await pg.query(`
        select
            id,
            case
                when id = 1 then 'Consolidado de kms recorrido por vehículo, flota y empresa.'
                when id = 2 then 'Cantidad de vehículos autorizados / Cantidad de vehículos en servicio.'
                when id = 3 then 'Antigüedad d los vehículos en servicio.'
                when id = 4 then 'Porcentaje de rendimiento de unidades, por empresa, por flota.'
                when id = 5 then 'Cantidad de vehículos autorizados por empresa / Cantidad de vehículos subsidiados.'
                when id = 6 then 'Cantidad de alertas de exceso de velocidad por unidad, por flota, por empresa.'
                when id = 7 then 'Cantidad de unidades / Cantidad de alertas de exceso de velocidad.'
                when id = 8 then 'Cantidad de unidades en geocerca, tramo / Día de la semana.'
                when id = 9 then 'Cantidad de unidades en geocerca, tramo / Hora del día.'
                when id = 10 then 'Consolidados de vehículos que no se detuvieron en paraderos autorizados.'
                when id = 11 then 'Ranking de unidades por empresa, por flota. Con mayor kilometraje obtenido según rango de fecha.'
                when id = 12 then 'Cantidad de vehículos por rango de velocidad x día x semana x tabla con baremos.'
                when id = 13 then 'Calidad del servicio de transporte por distrito.'
                when id = 14 then 'Tiempos de recorrido entre paraderos y rutas completadas por unidad, independientemente si el vehículo se detiene o no en el paradero .'
                when id = 15 then 'Promedio de tiempos entre paraderos y rutas completadas por empresa, independientemente si el vehículo se detiene o no en el paradero.'
                when id = 16 then 'Porcentaje de unidades por empresa que cumplen con los objetivos de subsidio.'
                when id = 17 then 'Cantidad de demanda de transporte público por horas, por días.'
                when id = 18 then 'Tiempos de unidad dentro de la ruta.'
                when id = 19 then 'Cantidad en años, meses, días de antigüedad de unidades de transporte.'
                when id = 20 then 'Cantidad de ms de recorrido de vida del vehículo de transporte.'
                when id = 21 then 'Promedio de ms recorridos por ruta, por flota, por vehículo.'
                when id = 22 then 'Cantidad de vehículos por geo carca, ruta, empresa, rango de fecha.'
                when id = 23 then 'Ranking de vehículos con mayor kilometraje dentro de ruta por rango de fecha.'
                when id = 24 then 'Ranking de unidades por empresa, por flota. Con mayor kilometraje obtenido según rango de fecha.'
            end as name
        from generate_series(1, 24) as id`, []);
        return res.status(200).send(camelcaseKeys(consulta));
    } catch (error) {
        return res.status(500).send({ message: error });
    }
}