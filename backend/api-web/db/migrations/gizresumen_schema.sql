--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.2
-- Dumped by pg_dump version 9.5.5

-- Started on 2021-10-27 19:46:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 9 (class 2615 OID 608189)
-- Name: giz_resumen; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA giz_resumen;


ALTER SCHEMA giz_resumen OWNER TO postgres;

SET search_path = giz_resumen, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 608193)
-- Name: giz_reporte01; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte01 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    placa_vehiculo character varying(10),
    salida_1_2 integer,
    viajes_incompletos_1_2 integer,
    viajes_completos_1_2 integer,
    salida_2_1 integer,
    viajes_completos_2_1 integer,
    viajes_incompletos_2_1 integer,
    p_viajes_completos_1_2 numeric(3,3),
    p_viajes_incompletos_2_1 numeric(3,3)
);


ALTER TABLE giz_reporte01 OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 634737)
-- Name: giz_reporte03; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte03 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    placa_vehiculo character varying(10),
    hora integer,
    cantidad integer,
    sentido integer,
    buses_en_serv_1_2 integer,
    buses_en_serv_2_1 integer,
    porc_transmision_1_2 numeric(3,3),
    porc_transmision_2_1 numeric(3,3)
);


ALTER TABLE giz_reporte03 OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 634700)
-- Name: giz_reporte04; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte04 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    placa_vehiculo character varying(10),
    longitud integer,
    n_paradas_ruta integer,
    n_viajes_completo integer,
    v_media_ruta12 integer,
    v_media_ruta21 integer
);


ALTER TABLE giz_reporte04 OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 634703)
-- Name: giz_reporte05; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte05 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    placa_vehiculo character varying(10),
    v_con_gps integer,
    v_sin_gps integer,
    v_servicio_c_gps integer,
    p_vehiculos_servicio_c_gps integer
);


ALTER TABLE giz_reporte05 OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 634706)
-- Name: giz_reporte06; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte06 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    placa_vehiculo character varying(10),
    v_con_gps integer,
    a_boton_panico integer,
    n_vehiculos_act_btn integer,
    p_vehiculos_servicio_c_gps numeric(3,3)
);


ALTER TABLE giz_reporte06 OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 634709)
-- Name: giz_reporte07; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte07 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    longitud_rutas integer,
    n_completos_ett integer,
    n_incompletos_ett integer,
    km_recorridos_con_ett integer,
    km_recorridos_inc_ett integer,
    tot_km_recorridos_s12 integer,
    porc_km_recorridos_s12 numeric(3,3)
);


ALTER TABLE giz_reporte07 OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 634712)
-- Name: giz_reporte08; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte08 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    longitud_rutas integer,
    n_paraderos_ruta integer,
    n_viajes_detenciones_100 integer,
    n_viajes_detenciones_inc integer,
    n_detenciones_no_realizadas_paraderos integer,
    porc_viajes_detenciones_paraderos_12 numeric(3,3)
);


ALTER TABLE giz_reporte08 OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 634715)
-- Name: giz_reporte09; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte09 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    porc_viajes_completos_12 numeric(3,3),
    porc_viajes_completos_21 numeric(3,3),
    porc_transmision_gps_min_12 numeric(3,3),
    velo_media_ruta12 integer,
    velo_media_ruta21 integer,
    porc_vehiculos_servicio_gps numeric(3,3),
    porc_alertas_panico_vehi_gps numeric(3,3),
    porc_km_recorridos12 numeric(3,3),
    porc_km_recorridos21 numeric(3,3),
    porc_viajes_detenciones_paraderos12 numeric(3,3),
    porc_viajes_detenciones_paraderos21 numeric(3,3),
    porc_km_recorridos_rutas_vs_gps numeric(3,3),
    pago_subsidio integer
);


ALTER TABLE giz_reporte09 OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 634719)
-- Name: giz_reporte10; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte10 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    km_recorridos_gps integer,
    km_recorridos_ruta integer,
    porc_km_recorridos_ruta_vs_gps numeric(3,3)
);


ALTER TABLE giz_reporte10 OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 634722)
-- Name: giz_reporte11; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte11 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    tot_km_recorridos integer,
    km_recorridos_ruta integer,
    pago_subsidio integer
);


ALTER TABLE giz_reporte11 OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 634725)
-- Name: giz_reporte12; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte12 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    km_recorridos_fuera_ruta integer
);


ALTER TABLE giz_reporte12 OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 634728)
-- Name: giz_reporte14; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte14 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    tiempo_sin_transmision_gps integer
);


ALTER TABLE giz_reporte14 OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 634731)
-- Name: giz_reporte15; Type: TABLE; Schema: giz_resumen; Owner: postgres
--

CREATE TABLE giz_reporte15 (
    fecha date,
    id_municipalidad character varying(6),
    id_ett integer,
    id_ruta integer,
    sentido integer,
    placa_vehiculo character varying(10),
    tot_tiempo_acumulado_o_s integer
);


ALTER TABLE giz_reporte15 OWNER TO postgres;

-- Completed on 2021-10-27 19:46:07

--
-- PostgreSQL database dump complete
--

