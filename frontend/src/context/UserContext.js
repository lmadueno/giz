import React, { useState, useEffect, createContext } from 'react';
import { Redirect } from 'react-router-dom';
import SesionService from '../services/SesionService';

const UserContext = createContext();

export const UserProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(undefined);

    useEffect(() => {
        const checkLoggedIn = async () => {
            let cuser = SesionService.isAuthenticated();
            if (cuser === null) {
                localStorage.setItem('user', '');
                cuser = '';
            }

            setCurrentUser(cuser);
        };

        checkLoggedIn();
    }, []);

    return (
        <UserContext.Provider value={[currentUser, setCurrentUser]}>
            {currentUser?.token ? children : <Redirect to="/login" />}
        </UserContext.Provider>
    );
};


export default UserContext;