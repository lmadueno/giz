import { useState, useEffect } from "react";

import { Link } from "react-router-dom";
import Button from '@mui/material/Button';
import BasicTable from '../../components/Table/Table';
import BackButton from '../../components/BackButton/BackButton';
import AlertaDet from "./AlertasDet";
import { useHistory } from "react-router-dom";

import AlertaService from "../../services/AlertaService";

const VISTA_ALERTAS = 'VISTA_ALERTAS';
const VISTA_ALERTA_DET = 'VISTA_ALERTA_DET';

export default function Alerta() {
    let history = useHistory();
    const [alertas, setAlertas] = useState([]);
    const [vista, setVista] = useState(VISTA_ALERTAS);

    useEffect(() => {
        const getAlertas = async () => {
            const alertas = await AlertaService.getAll();
            setAlertas(alertas.data);
        };
        getAlertas();
    }, []);

    const handleClickAlerta = (e, id) => {
        setVista(VISTA_ALERTA_DET);
        history.push(`/alertas/1`);
    }

    const handleClickBack = () => {
        setVista(VISTA_ALERTAS);
    }

    const columns = [
        {
            Header: 'Alertas',
            columns: [
                {
                    Header: '#',
                    accessor: 'idAlerta',
                },
                {
                    Header: 'Alerta',
                    accessor: 'nombreAlerta',
                },
                {
                    Header: "Visualizar",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { idAlerta } = alertas[rowIdx];

                        return (
                            <Button variant="contained" size="small" onClick={(e) => handleClickAlerta(e, idAlerta)}>
                                Ver
                            </Button>
                        );
                    }
                }
            ],
        },
    ];

    if (vista === VISTA_ALERTAS)
        return (
            <BasicTable
                columns={columns}
                data={alertas}
                sizePro='small'
            />
        );

    if (vista === VISTA_ALERTA_DET)
        return (
            <>
                <BackButton handleClickBack={handleClickBack} />
                <br />
                <br />
                <AlertaDet />
            </>
        );
}