import { useState, useEffect, Fragment } from 'react';
import { GeoJSON } from '@monsonjeremy/react-leaflet';
import Map from '../../components/Map/Map';
import AlertaService from '../../services/AlertaService';
import RutaService from '../../services/RutaService';
import { STYLE_GEOM_2_1 } from '../../components/Poligonos/RutasPoligono';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function AlertaDet() {
    const [alertas, setAlertas] = useState([]);
    const [rutas, setRutas] = useState([]);
    const [ruta, setRuta] = useState(-1);
    const [geom, setGeom] = useState({});

    useEffect(() => {
        const getRutas = async () => {
            let rutasPorSentido = [];
            const rutas = await RutaService.getAll();

            rutas.data.forEach(ruta => {
                const geomSentido12 = ruta.geom12;
                const geomSentido21 = ruta.geom21;

                const { geom12, geom21, ...rutaSinSentidos } = ruta;

                rutasPorSentido.push({
                    ...rutaSinSentidos,
                    nombreRuta: `${ruta.nombreRuta} sentido 1-2`,
                    geom: geomSentido12
                });

                rutasPorSentido.push({
                    ...rutaSinSentidos,
                    nombreRuta: `${ruta.nombreRuta} sentido 2-1`,
                    geom: geomSentido21
                })
            });

            setRutas(rutasPorSentido);
        };
        getRutas();
    }, []);

    const handleChangeRuta = (e) => {
        const idxRuta = e.target.value;
        const { idRuta, geom } = rutas[idxRuta];
        setRuta(idxRuta);
        setGeom(geom);
        getAlertas(idRuta, geom);
    }

    const getAlertas = async (idRuta, geom) => {
        const alerta = await AlertaService.get(idRuta, geom);
        setAlertas(alerta.data);
    };

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={3}>
                        <FormControl margin="dense" fullWidth>
                            <InputLabel>Rutas</InputLabel>
                            <Select label="Rutas" value={ruta} onChange={handleChangeRuta}>
                                {
                                    rutas.map((ruta, index) => (
                                        <MenuItem key={ruta.nombreRuta} value={index}>{ruta.nombreRuta}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </Box>

            <Map>
                {
                    geom?.coordinates && <GeoJSON key={geom.coordinates} data={geom} style={STYLE_GEOM_2_1}></GeoJSON>
                }

                {
                    alertas && alertas.map(({ geojson }, index) => (
                        <GeoJSON key={index} data={geojson}></GeoJSON>
                    ))
                }
            </Map>
        </>
    )
}