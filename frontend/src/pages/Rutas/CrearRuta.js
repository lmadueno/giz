import { useFormik } from 'formik';
import { styled } from '@mui/material/styles';

import * as yup from 'yup';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import RutaService from '../../services/RutaService';
import FormControl from '@mui/material/FormControl';
import UploadFile from '@mui/icons-material/UploadFile';
import Delete from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Map from '../../components/Map/Map';
import { GeoJSON } from '@monsonjeremy/react-leaflet';
import { CREAR_RUTA } from './Rutas';
import { jsonIsValid } from '../../helper/helper';

const Input = styled('input')({
    display: 'none',
});

const validationSchema = yup.object({
    codigoRuta: yup
        .string('Ingresa el código de ruta')
        .required('Nombre de ruta es requerido'),
    nombreRuta: yup
        .string('Ingresa el nombre de la ruta')
        .required('Nombre de Ruta es requerido'),
});

const GEOM_1_2 = 'geom12';
const GEOM_2_1 = 'geom21';

export default function CrearRuta({ formulario, initialValues, onSuccess, onError }) {
    const formik = useFormik({
        initialValues: {
            idRuta: initialValues?.idRuta || 0,
            codigoRuta: initialValues?.codigoRuta || '',
            nombreRuta: initialValues?.nombreRuta || '',
            glosaRuta: initialValues?.glosaRuta || '',
            detalleRuta: initialValues?.detalleRuta || '',
            geom12: initialValues?.geom12 || '',
            geom21: initialValues?.geom21 || ''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            postRuta({
                ...values,
                geom12: jsonIsValid(values.geom12) ? JSON.parse(values.geom12) : '',
                geom21: jsonIsValid(values.geom21) ? JSON.parse(values.geom21) : '',
            });
        },
    });

    const postRuta = async (body) => {
        try {
            const res = formulario === CREAR_RUTA ? await RutaService.post(body) : await RutaService.put(body);
            onSuccess(res.data);
        } catch (error) {
            onError(error);
        }
    }

    const handleChangeFile = (e, field = GEOM_1_2) => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            const jsonStr = e.target.result;
            if (jsonIsValid(jsonStr))
                formik.setFieldValue(field, jsonStr);
        };
    };

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <form onSubmit={formik.handleSubmit}>
                            <TextField
                                autoFocus
                                fullWidth
                                margin="dense"
                                id="codigoRuta"
                                name="codigoRuta"
                                label="Codigo Ruta"
                                value={formik.values.codigoRuta}
                                onChange={formik.handleChange}
                                error={formik.touched.codigoRuta && Boolean(formik.errors.codigoRuta)}
                                helperText={formik.touched.codigoRuta && formik.errors.codigoRuta}
                            />
                            <TextField
                                fullWidth
                                margin="dense"
                                id="nombreRuta"
                                name="nombreRuta"
                                label="Nombre Ruta"
                                value={formik.values.nombreRuta}
                                onChange={formik.handleChange}
                                error={formik.touched.nombreRuta && Boolean(formik.errors.nombreRuta)}
                                helperText={formik.touched.nombreRuta && formik.errors.nombreRuta}
                            />
                            <TextField
                                fullWidth
                                margin="dense"
                                id="glosaRuta"
                                name="glosaRuta"
                                label="Glosa Ruta"
                                value={formik.values.glosaRuta}
                                onChange={formik.handleChange}
                                error={formik.touched.glosaRuta && Boolean(formik.errors.glosaRuta)}
                                helperText={formik.touched.glosaRuta && formik.errors.glosaRuta}
                            />
                            <TextField
                                fullWidth
                                margin="dense"
                                id="detalleRuta"
                                name="detalleRuta"
                                label="Detalle Ruta"
                                value={formik.values.detalleRuta}
                                onChange={formik.handleChange}
                                error={formik.touched.detalleRuta && Boolean(formik.errors.detalleRuta)}
                                helperText={formik.touched.detalleRuta && formik.errors.detalleRuta}
                            />

                            <FormControl fullWidth margin="dense">
                                <Box sx={{ flexGrow: 1 }}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={6}>
                                            {jsonIsValid(formik.values.geom12) &&
                                                <Button variant="contained" fullWidth endIcon={<Delete />} onClick={() => formik.setFieldValue(GEOM_1_2, '')}>Eliminar sentido 1-2</Button>
                                            }
                                            {!jsonIsValid(formik.values.geom12) &&
                                                <label htmlFor="adjuntar-sentido-1-2" >
                                                    <Input accept=".json" id="adjuntar-sentido-1-2" type="file" onChange={(e) => handleChangeFile(e, GEOM_1_2)} />
                                                    <Button variant="outlined" fullWidth component="span" endIcon={<UploadFile />}>
                                                        Aduntar sentido 1-2
                                                    </Button>
                                                </label>
                                            }
                                        </Grid>
                                        <Grid item xs={6}>
                                            {jsonIsValid(formik.values.geom21) &&
                                                <Button variant="contained" fullWidth endIcon={<Delete />} onClick={() => formik.setFieldValue(GEOM_2_1, '')}>Eliminar sentido ruta21</Button>
                                            }
                                            {!jsonIsValid(formik.values.geom21) &&
                                                <label htmlFor="adjuntar-sentido-2-1" >
                                                    <Input accept=".json" id="adjuntar-sentido-2-1" type="file" onChange={(e) => handleChangeFile(e, GEOM_2_1)} />
                                                    <Button variant="outlined" fullWidth component="span" endIcon={<UploadFile />}>
                                                        Aduntar sentido 2-1
                                                    </Button>
                                                </label>
                                            }
                                        </Grid>
                                    </Grid>
                                </Box>
                            </FormControl>

                            <Button color="primary" variant="contained" fullWidth type="submit">
                                Guardar
                            </Button>

                        </form>
                    </Grid>
                    <Grid item xs={6}>
                        <Map height="100%">
                            {jsonIsValid(formik.values.geom12) && <GeoJSON data={JSON.parse(formik.values.geom12)}></GeoJSON>}
                            {jsonIsValid(formik.values.geom21) && <GeoJSON data={JSON.parse(formik.values.geom21)}></GeoJSON>}
                        </Map>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}