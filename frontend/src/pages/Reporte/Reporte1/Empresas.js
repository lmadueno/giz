import { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Paper from '@mui/material/Paper';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas(empresas.data.detalle);
            setResumen(empresas.data.resumen);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte de Viajes Completos/Incompletos por Empresas de Transporte Registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Empresa",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    }
                },
                {
                    Header: 'Rutas',
                    accessor: 'rutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses',
                    accessor: 'buses',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de salidas sentido 1-2',
                    accessor: 'salidas12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes incompletos sentido 1-2',
                    accessor: 'viajesIncompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes completos sentido 1-2',
                    accessor: 'viajesCompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de salidas sentido 2-1',
                    accessor: 'salidas21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes incompletos sentido 2-1',
                    accessor: 'viajesIncompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes completos sentido 2-1',
                    accessor: 'viajesCompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 1-2',
                    accessor: 'porcViajesCompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 2-1',
                    accessor: 'porcViajesCompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center',
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas} />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto'
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}