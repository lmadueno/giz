import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Rutas({ reporteId, empresaId, handleClickRuta }) {
    const [rutas, setRutas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await ReporteService.getRutas(reporteId, empresaId);
            setRutas(rutas.data.detalle);
            setResumen(rutas.data.resumen);
        };
        getRutas();
    }, [reporteId, empresaId]);

    const columns = [
        {
            Header: 'Reporte de Viajes Completos/Incompletos por Ruta y por Empresa',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                    align: 'center'
                },
                {
                    Header: "Ruta",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, ruta } = rutas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickRuta(e, id)
                            }}>
                                {ruta}
                            </Link>
                        );
                    },
                    align: 'center'
                },
                {
                    Header: 'Buses',
                    accessor: 'buses',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de salidas sentido 1-2',
                    accessor: 'salidas12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes incompletos sentido 1-2',
                    accessor: 'viajesIncompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes completos sentido 1-2',
                    accessor: 'viajesCompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de salidas sentido 2-1',
                    accessor: 'salidas21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes incompletos sentido 2-1',
                    accessor: 'viajesIncompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° viajes completos sentido 2-1',
                    accessor: 'viajesCompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 1-2',
                    accessor: 'porcViajesCompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 2-1',
                    accessor: 'porcViajesCompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center',
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columns} data={rutas} />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto'
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}