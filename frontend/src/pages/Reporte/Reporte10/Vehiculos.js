import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos, setVehiculos] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos(vehiculos.data.detalle);
            setResumen(vehiculos.data.resumen);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const columns =
    {
        Header: 'Detalle Ruta 1 Sentido 1-2',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: "placa",
                align: 'center'
            },
            {
                Header: 'Kilómetros Recorridos según GPS',
                accessor: 'kmRecorridosGps',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Kilómetros Recorridos según Ruta',
                accessor: 'kmRecorridosRuta',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: '% de Km Recorridos Rutas vs.GPS',
                accessor: 'porcKmRecorridosRutaVsGps',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}