import { useState } from 'react';
import Empresas from './Empresas';

import BackButton from '../../../components/BackButton/BackButton';

const VISTA_EMPRESAS = 'VISTA_EMPRESAS';

export default function Reporte9({ reporteId }) {
    const [empresaId, setEmpresaId] = useState(0);
    const [vista, setVista] = useState(VISTA_EMPRESAS);

    const handleClickEmpresa = (e, id) => {
        e.preventDefault();
        setEmpresaId(id);
    }


    if (vista === VISTA_EMPRESAS)
        return (
            <>
                <BackButton to='/reportes' />
                <Empresas reporteId={reporteId} handleClickEmpresa={handleClickEmpresa} />
            </>
        );



}