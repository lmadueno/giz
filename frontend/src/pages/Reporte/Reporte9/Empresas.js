import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas12] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas12(empresas.data.detalle);
            console.log(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte de los Indicadores de Empresas de Transporte Registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Empresa de Transporte",
                    accessor: 'empresa',

                },
                {
                    Header: 'Rutas',
                    accessor: 'nRutas',
                },
                {
                    Header: 'N° de Buses Autorizados',
                    accessor: 'nBusesAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 1-2',
                    accessor: 'porcViajesCompletos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Viajes completos Sentido 2-1',
                    accessor: 'porcViajesCompletos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 1-2',
                    accessor: 'porcTransmisionGpsMin12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 2-1',
                    accessor: 'porcTransmisionGpsMin21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Velocidad Media por Ruta Sentido 1-2',
                    accessor: 'veloMediaRuta12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Velocidad Media por Ruta. Sentido 2-1',
                    accessor: 'veloMediaRuta21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Vehículos en Servicio con GPS',
                    accessor: 'porcVehiculosServicioGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Alertas de Panico por Vehículos con GPS',
                    accessor: 'porcAlertasPanicoVehiGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Kilómetros Recorridos Sentido 1-2',
                    accessor: 'porcKmRecorridos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Kilómetros Recorridos Sentido 2-1',
                    accessor: 'porcKmRecorridos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '%  de Viajes con Detenciones de Vehículos en Paraderos Sentido 1-2',
                    accessor: 'porcViajesDetencionesParaderos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '%  de Viajes con Detenciones de Vehículos en Paraderos Sentido 2-1',
                    accessor: 'porcViajesDetencionesParaderos21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Km Recorridos Rutas vs. GPS',
                    accessor: 'porcKmRecorridosRutasVsGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Pago de Subsidio (S/)',
                    accessor: 'pagoSubsidio',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Kilómetros Recorridos fuera de Ruta (según GPS)',
                    accessor: 'kmRecorridosFueraRuta',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Tiempo sin transmisión de GPS (dias, horas, minutos)',
                    accessor: 'tSinTransmisionGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Total Tiempo Acumulado en la Operación del Servicio (días y horas)',
                    accessor: 'tTiempoAcuOperaServicio',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas} />
            <br />

        </>
    );
}