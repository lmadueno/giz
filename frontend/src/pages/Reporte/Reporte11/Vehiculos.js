import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos, setVehiculos] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos(vehiculos.data.detalle);
            setResumen(vehiculos.data.resumen);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const columns =
    {
        Header: 'Reporte de Vehículos que cumplen con kilómetros válidos para Subsidio, por Tipo de Vehículo',
        columns: [
            {
                Header: '#',
                accessor: 'id',
                align: 'center'
            },
            {
                Header: "Placa",
                accessor: "placa",
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Tipo Vehiculo',
                accessor: 'tipoVehiculo',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Subsidio por km (S/)',
                accessor: 'subsidioKm',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Total Kilómetros Recorridos',
                accessor: 'totKmRecorridos',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Pago de Subsidio (S/)',
                accessor: 'pagoSubsidio',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}