import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Rutas({ reporteId, empresaId, handleClickRuta }) {
    const [rutas, setRutas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await ReporteService.getRutas(reporteId, empresaId);
            setRutas(rutas.data.detalle);
            setResumen(rutas.data.resumen);
        };
        getRutas();
    }, [reporteId, empresaId]);

    const columns = [
        {
            Header: 'Reporte de Viajes Completos/Incompletos por Ruta y por Empresa',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                    align: 'center'

                },
                {
                    Header: "Ruta",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, ruta } = rutas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickRuta(e, id)
                            }}>
                                {ruta}
                            </Link>
                        );
                    },
                    align: 'center'
                },
                {
                    Header: 'Buses Autorizados (al dìa)',
                    accessor: 'busesAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses en Servicio Sentido 1-2',
                    accessor: 'busesEnServ12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses en Servicio Sentido 2-1',
                    accessor: 'busesEnServ21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 1-2',
                    accessor: 'porcTransmision12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 2-1',
                    accessor: 'porcTransmision21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columns} data={rutas} sizePro='small' />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto'
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}