import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos12, setVehiculos12] = useState([]);
    const [vehiculos21, setVehiculos21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos12(vehiculos.data.sentido12);
            setVehiculos21(vehiculos.data.sentido21);
            setResumen(vehiculos.data.resumen);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const hourColumns = [...Array(25).keys()].filter(hour => hour >= 5).map(hour => {
        return {
            Header: `H-${hour}`,
            accessor: `h${hour}`,
            alignBody: 'center',
            alignHeader: 'center'
        }
    });

    const columns12 =
    {
        Header: 'Detalle Ruta 1 Sentido 1-2',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: 'placa',
            },
            ...hourColumns,
            {
                Header: "% de transmisión del GPS por minuto Sentido 1-2",
                accessor: 'porcSentido12',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    const columns21 =
    {
        Header: 'Detalle Ruta 1 Sentido 2-1',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: 'placa',
            },
            ...hourColumns,
            {
                Header: "% de transmisión del GPS por minuto Sentido 2-1",
                accessor: 'porcSentido21',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable isExportable columns={[columns12]} data={vehiculos12} />
            <br />
            <BasicTable isExportable columns={[columns21]} data={vehiculos21} />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto'
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}