import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas(empresas.data.detalle);
            setResumen(empresas.data.resumen);
        };
        getEmpresas();
    }, [reporteId]);

    const columns = [
        {
            Header: 'Reporte de Viajes Completos/Incompletos por Empresas de Transporte Registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                    align: 'center'
                },
                {
                    Header: "Empresa de Transporte",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    },
                },
                {
                    Header: 'Rutas',
                    accessor: 'rutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses Autorizados (al dìa)',
                    accessor: 'busesAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses en Servicio Sentido 1-2',
                    accessor: 'busesEnServ12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Buses en Servicio Sentido 2-1',
                    accessor: 'busesEnServ21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 1-2',
                    accessor: 'porcTransmision12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Transmisión del GPS por minuto Sentido 2-1',
                    accessor: 'porcTransmision21',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columns} data={empresas} sizePro='small' />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto'
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                alignBody: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}