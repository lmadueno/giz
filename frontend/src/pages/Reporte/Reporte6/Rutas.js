import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Rutas({ reporteId, empresaId, handleClickRuta }) {
    const [rutas, setRutas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await ReporteService.getRutas(reporteId, empresaId);
            setRutas(rutas.data.detalle);
            setResumen(rutas.data.resumen);
            console.log(rutas.data);
        };
        getRutas();
    }, [reporteId, empresaId]);

    const columns = [
        {
            Header: 'Reporte de Vehículos con GPS por Ruta y EETT',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Ruta",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, ruta } = rutas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickRuta(e, id)
                            }}>
                                {ruta}
                            </Link>
                        );
                    }
                },
                {
                    Header: 'Vehículos Autorizados',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Vehículos con GPS',
                    accessor: 'vConGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Alertas de Botón de Pánico',
                    accessor: 'aBotonPanico',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Vehículos que Activaron Botón de Pánico',
                    accessor: 'nVehiculosActBtn',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Alertas de Panico por Vehículos con GPS',
                    accessor: 'pVehiculosServicioGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable
                isExportable
                columns={columns}
                data={rutas}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                alignBody: 'center',
                                alignHeader: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}