import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas(empresas.data.detalle);
            setResumen(empresas.data.resumen);
            console.log(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte de Alertas de Pánico en las Empresas de Transporte registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                    align: 'center'

                },
                {
                    Header: "Empresa de Transporte",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    },
                    align: 'center'

                },
                {
                    Header: 'Vehículos Autorizados',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'

                },
                {
                    Header: 'Vehículos con GPS',
                    accessor: 'vConGps',
                    alignBody: 'center',
                    alignHeader: 'center'

                },
                {
                    Header: 'Alertas de Botón de Pánico',
                    accessor: 'aBotonPanico',
                    alignBody: 'center',
                    alignHeader: 'center'

                },
                {
                    Header: 'N° Vehículos que Activaron Botón de Pánico',
                    accessor: 'nVehiculosActBtn',
                    alignBody: 'center',
                    alignHeader: 'center'

                },
                {
                    Header: '% de Alertas de Panico por Vehículos con GPS',
                    accessor: 'pVehiculosServicioCGps',
                    alignBody: 'center',
                    alignHeader: 'center'

                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas} />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                                align: 'center'

                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'

                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}