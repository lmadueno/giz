import { useState } from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

import Rutas from './Rutas';
import Empresas from './Empresas';
import Vehiculos from './Vehiculos'

import BackButton from '../../../components/BackButton/BackButton';

const VISTA_EMPRESAS = 'VISTA_EMPRESAS';
const VISTA_RUTAS = 'VISTA_RUTAS';
const VISTA_VEHICULOS = 'VISTA_VEHICULOS';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function Reporte6({ reporteId }) {
    const [empresaId, setEmpresaId] = useState(0);
    const [rutaId, setRutaId] = useState(0);
    const [vista, setVista] = useState(VISTA_EMPRESAS);

    const handleClickBack = () => {
        if (vista === VISTA_RUTAS) setVista(VISTA_EMPRESAS);
        if (vista === VISTA_VEHICULOS) setVista(VISTA_RUTAS);
    }

    const handleClickEmpresa = (e, id) => {
        e.preventDefault();
        setEmpresaId(id);
        setVista(VISTA_RUTAS);
    }

    const handleClickRuta = (e, id) => {
        e.preventDefault();
        setRutaId(id);
        setVista(VISTA_VEHICULOS);
    }

    if (vista === VISTA_EMPRESAS)
        return (
            <>
                <BackButton to='/reportes' />
                <Empresas reporteId={reporteId} handleClickEmpresa={handleClickEmpresa} />
            </>
        );

    if (vista === VISTA_RUTAS)
        return (
            <>
                <BackButton handleClickBack={handleClickBack} />
                <Rutas reporteId={reporteId} empresaId={empresaId} handleClickRuta={handleClickRuta} />
            </>
        );

    if (vista === VISTA_VEHICULOS)
        return (
            <>
                <BackButton handleClickBack={handleClickBack} />
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Item>
                                <Vehiculos />
                            </Item>
                        </Grid>
                    </Grid>
                </Box>
            </>
        );
}