import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas(empresas.data.detalle);
            setResumen(empresas.data.resumen);
            console.log(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte de Vehículos con GPS por Empresas de Transporte registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Empresa de Transporte",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    },
                   
                },
                {
                    Header: 'Vehículos Autorizados',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Vehículos con GPS',
                    accessor: 'vConGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Vehículos sin GPS',
                    accessor: 'vSinGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Vehículos en Servicio con GPS',
                    accessor: 'vServicioCGps',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% de Vehículos en Servicio con GPS',
                    accessor: 'pVehiculosServicioCGps',
                    alignBody: 'center',
            alignHeader: 'center'
                },
            ],
            alignHeader:'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas} sizePro='small'/>
            <br />

            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
                sizePro='small'
            />
        </>
    );
}