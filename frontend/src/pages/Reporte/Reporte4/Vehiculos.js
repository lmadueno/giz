import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos12, setVehiculos12] = useState([]);
    const [vehiculos21, setVehiculos21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos12(vehiculos.data.sentido12);
            setVehiculos21(vehiculos.data.sentido21);
            setResumen(vehiculos.data.resumen);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const hourColumns = [...Array(23).keys()].filter(hour => hour >= 5).map(hour => {
        return {
            Header: `H-${hour} Velo. Km/h`,
            accessor: `h${hour}`,
            alignBody: 'center',
            alignHeader: 'center'
        }
    });

    const columns =
    {
        Header: 'Detalle Ruta 1 Sentido 1-2',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: 'placa',
                align: 'center'
            },
            ...hourColumns,
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos12}
            />
            <br />
            <BasicTable
                isExportable
                columns={[{
                    ...columns,
                    Header: 'Detalle Ruta 1 Sentido 2-1',
                }]}
                data={vehiculos21}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',

                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                                align: 'center'
                            },
                        ]
                    }
                ]}
                data={resumen}
            />

        </>
    );
}