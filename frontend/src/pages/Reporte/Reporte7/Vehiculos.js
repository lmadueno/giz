import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos12, setVehiculos12] = useState([]);
    const [vehiculos21, setVehiculos21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos12(vehiculos.data.sentido12);
            setVehiculos21(vehiculos.data.sentido21);
            setResumen(vehiculos.data.resumen);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const columns =
    {
        Header: 'Detalle Ruta 1 Sentido 1-2',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: "placa",
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'N° Viajes incompletos de la EETT',
                accessor: 'nCompletosEtt',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'N° Viajes incompletos de la EETT',
                accessor: 'nIncompletosEtt',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Kilómetros Recorridos completos de la EETT',
                accessor: 'kmRecorridosConEtt',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Kilómetros Recorridos incompletos de la EETT',
                accessor: 'kmRecorridosIncEtt',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: 'Total Kilómetros Recorridos Sentido 1-2',
                accessor: 'totKmRecorridosS12',
                alignBody: 'center',
                alignHeader: 'center'
            },
            {
                Header: '% Kilómetros Recorridos Sentido 1-2',
                accessor: 'porcKmRecorridosS12',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos12}
            />
            <br />

            <BasicTable
                isExportable
                columns={[{
                    ...columns,
                    Header: 'Detalle Ruta 1 Sentido 2-1',
                }]}
                data={vehiculos21}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}