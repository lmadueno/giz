import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas12, setEmpresas12] = useState([]);
    const [empresas21, setEmpresas21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas12(empresas.data.sentido_1_2);
            setEmpresas21(empresas.data.sentido_2_1);
            setResumen(empresas.data.resumen);
            console.log(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte del Historial Geográfico del recorrido de las Rutas de las Empresas de Transporte Registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Buses Autorizados a la EETT",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas12[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    }
                },
                {
                    Header: 'Longitud de las Rutas de la EETT (km)',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes completos de la EETT',
                    accessor: 'longitudRutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes incompletos de la EETT',
                    accessor: 'nCompletosEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes incompletos de la EETT',
                    accessor: 'nIncompletosEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Kilómetros Recorridos completos de la EETT',
                    accessor: 'kmRecorridosConEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Kilómetros Recorridos incompletos de la EETT',
                    accessor: 'kmRecorridosIncEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Total Kilómetros Recorridos Sentido 1-2',
                    accessor: 'totKmRecorridosS12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Kilómetros Recorridos Sentido 1-2',
                    accessor: 'porcKmRecorridosS12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader:'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas12} sizePro='small'/>
            <br />
            <BasicTable isExportable columns={columnsDet} data={empresas21} sizePro='small'/>
            <br />

            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
                sizePro='small'
            />
        </>
    );
}