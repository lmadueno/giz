import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Rutas({ reporteId, empresaId, handleClickRuta }) {
    const [rutas12, setRutas12] = useState([]);
    const [rutas21, setRutas21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await ReporteService.getRutas(reporteId, empresaId);
            setRutas12(rutas.data.sentido_1_2);
            setRutas21(rutas.data.sentido_2_1);
            setResumen(rutas.data.resumen);
            console.log(rutas.data);
        };
        getRutas();
    }, [reporteId, empresaId]);

    const columns = [
        {
            Header: 'Reporte de Vehículos con GPS por Ruta y EETT',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Ruta",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, ruta } = rutas12[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickRuta(e, id)
                            }}>
                                {ruta}
                            </Link>
                        );
                    },
                    align: 'center'
                },
                {
                    Header: 'Longitud de las Rutas de la EETT (km)',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes completos de la EETT',
                    accessor: 'longitudRutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes incompletos de la EETT',
                    accessor: 'nCompletosEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Viajes incompletos de la EETT',
                    accessor: 'nIncompletosEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Kilómetros Recorridos completos de la EETT',
                    accessor: 'kmRecorridosConEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Kilómetros Recorridos incompletos de la EETT',
                    accessor: 'kmRecorridosIncEtt',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Total Kilómetros Recorridos Sentido 1-2',
                    accessor: 'totKmRecorridosS12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '% Kilómetros Recorridos Sentido 1-2',
                    accessor: 'porcKmRecorridosS12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable
                isExportable
                columns={columns}
                data={rutas12}
            />
            <br />
            <BasicTable
                isExportable
                columns={columns}
                data={rutas21}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}