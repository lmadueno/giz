import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos, setVehiculos] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos(vehiculos.data.detalle);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const columns =
    {
        Header: 'Reporte del Tiempo sin transmisión de GPS en Operación del Servicio, por Vehículo',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: "placa",
            },
            {
                Header: 'Tiempo sin transmisión de GPS (dias, horas, minutos)',
                accessor: 'tiempoSinTransmisionGps',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos}
            />
        </>
    );
}