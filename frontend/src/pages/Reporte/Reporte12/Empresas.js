import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas, setEmpresas] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte de Kilómetros Recorridos por Vehículos fuera de la Ruta y en horario del Servicio por Empresa de Transporte registrada',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Buses Autorizados a la EETT",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    }
                },
                {
                    Header: 'Kilómetros Recorridos fuera de Ruta (según GPS)',
                    accessor: 'kmRecorridosFueraRuta',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas} />
        </>
    );
}