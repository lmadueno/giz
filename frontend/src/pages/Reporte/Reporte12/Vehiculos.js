import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';

export default function Rutas({ reporteId, empresaId, rutaId, setShowVehiculos }) {
    const [vehiculos, setVehiculos] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getVehiculos = async () => {
            const vehiculos = await ReporteService.getVehiculos(reporteId, empresaId, rutaId);
            setVehiculos(vehiculos.data.detalle);
        };
        getVehiculos();
    }, [reporteId, empresaId, rutaId]);

    const columns =
    {
        Header: 'Detalle Ruta 1 Sentido 1-2',
        columns: [
            {
                Header: '#',
                accessor: 'id',
            },
            {
                Header: "Placa",
                accessor: "placa",
            },
            {
                Header: 'Kilómetros Recorridos fuera de Ruta (según GPS)',
                accessor: 'kmRecorridosFueraRuta',
                alignBody: 'center',
                alignHeader: 'center'
            },
        ],
        alignHeader: 'center'
    };

    return (
        <>
            <BasicTable
                isExportable
                columns={[columns]}
                data={vehiculos}
            />
        </>
    );
}