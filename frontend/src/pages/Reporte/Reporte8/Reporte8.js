import { useState } from 'react';
import Rutas from './Rutas';
import Empresas from './Empresas';

import BackButton from '../../../components/BackButton/BackButton';

const VISTA_EMPRESAS = 'VISTA_EMPRESAS';
const VISTA_RUTAS = 'VISTA_RUTAS';

export default function Reporte8({ reporteId }) {
    const [empresaId, setEmpresaId] = useState(0);
    const [rutaId, setRutaId] = useState(0);
    const [vista, setVista] = useState(VISTA_EMPRESAS);
    const handleClickBack = () => {
        if (vista === VISTA_RUTAS) setVista(VISTA_EMPRESAS);
        // if (vista === VISTA_VEHICULOS) setVista(VISTA_RUTAS);
    }

    const handleClickEmpresa = (e, id) => {
        e.preventDefault();
        setEmpresaId(id);
        setVista(VISTA_RUTAS);
    }

    const handleClickRuta = (e, id) => {
        e.preventDefault();
        setRutaId(id);
    }

    if (vista === VISTA_EMPRESAS)
        return (
            <>
                <BackButton to='/reportes' />
                <Empresas reporteId={reporteId} handleClickEmpresa={handleClickEmpresa} />
            </>
        );

    if (vista === VISTA_RUTAS)
        return (
            <>
                <BackButton handleClickBack={handleClickBack} />
                <Rutas reporteId={reporteId} empresaId={empresaId} handleClickRuta={handleClickRuta} />
            </>
        );


}