import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Empresas({ reporteId, handleClickEmpresa }) {
    const [empresas12, setEmpresas12] = useState([]);
    const [empresas21, setEmpresas21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getEmpresas = async () => {
            const empresas = await ReporteService.getEmpresas(reporteId);
            setEmpresas12(empresas.data.sentido_1_2);
            setEmpresas21(empresas.data.sentido_2_1);
            setResumen(empresas.data.resumen);
            console.log(empresas.data.detalle);
        };
        getEmpresas();
    }, [reporteId]);

    const columnsDet = [
        {
            Header: 'Reporte del Cumplimiento de Detención de Vehículos en Paraderos en las Empresas de Transporte registradas',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: "Empresa de Transporte",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id, empresa } = empresas12[rowIdx];

                        return (
                            <Link href="#" underline="none" onClick={(e) => {
                                handleClickEmpresa(e, id)
                            }}>
                                {empresa}
                            </Link>
                        );
                    }
                },
                {
                    Header: 'N° de Rutas',
                    accessor: 'nRutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Buses Autorizados',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Longitud de la Ruta (km)',
                    accessor: 'longitudRutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Paraderos en Ruta',
                    accessor: 'nParaderosRuta',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Viajes con Detenciones al 100%',
                    accessor: 'nViajesDetenciones100',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Viajes con Detenciones Incompletas',
                    accessor: 'nViajesDetencionesInc',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Detenciones No Realizadas en Paraderos ',
                    accessor: 'nDetencionesNoRealizadasParaderos',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '%  de Viajes con Detenciones de Vehículos en Paraderos Sentido 1-2',
                    accessor: 'porcViajesDetencionesParaderos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable isExportable columns={columnsDet} data={empresas12} />
            <br />
            <BasicTable isExportable columns={columnsDet} data={empresas21} />
            <br />

            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}