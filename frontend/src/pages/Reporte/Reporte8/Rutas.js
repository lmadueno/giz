import { useState, useEffect } from 'react';
import BasicTable from '../../../components/Table/Table';
import ReporteService from '../../../services/ReporteService';
import Link from '@mui/material/Link';

export default function Rutas({ reporteId, empresaId, handleClickRuta }) {
    const [rutas12, setRutas12] = useState([]);
    const [rutas21, setRutas21] = useState([]);
    const [resumen, setResumen] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await ReporteService.getRutas(reporteId, empresaId);
            setRutas12(rutas.data.sentido_1_2);
            setRutas21(rutas.data.sentido_2_1);
            setResumen(rutas.data.resumen);
            console.log(rutas.data);
        };
        getRutas();
    }, [reporteId, empresaId]);

    const columns = [
        {
            Header: 'Reporte del Cumplimiento de Detención de Vehículos en Paraderos por Ruta y por Sentidos',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: "Ruta",
                    accessor: 'ruta',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° Buses Autorizados',
                    accessor: 'vAutorizados',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Longitud de la Ruta (km)',
                    accessor: 'longitudRutas',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Paraderos en Ruta',
                    accessor: 'nParaderosRuta',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Viajes con Detenciones al 100%',
                    accessor: 'nViajesDetenciones100',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Viajes con Detenciones Incompletas',
                    accessor: 'nViajesDetencionesInc',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'N° de Detenciones No Realizadas en Paraderos ',
                    accessor: 'nDetencionesNoRealizadasParaderos',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: '%  de Viajes con Detenciones de Vehículos en Paraderos Sentido 1-2',
                    accessor: 'porcViajesDetencionesParaderos12',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
            ],
            alignHeader: 'center'
        },
    ];

    return (
        <>
            <BasicTable
                isExportable
                columns={columns}
                data={rutas12}
            />
            <br />
            <BasicTable
                isExportable
                columns={columns}
                data={rutas21}
            />
            <br />
            <BasicTable
                isExportable
                columns={[
                    {
                        Header: 'Tabla Resumen',
                        columns: [
                            {
                                Header: 'Concepto',
                                accessor: 'concepto',
                            },
                            {
                                Header: 'Porcentaje',
                                accessor: 'porcentaje',
                            },
                        ]
                    }
                ]}
                data={resumen}
            />
        </>
    );
}