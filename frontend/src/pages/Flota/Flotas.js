import { useState, useEffect } from "react";
import { useParams } from "react-router";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import BasicTable from '../../components/Table/Table';
import Dialog from '../../components/Dialog/Dialog';
import Notification from '../../components/Notification/Notification';
import FlotaService from '../../services/FlotaService';
import useStyle from './style';
import CrearFlota from './CrearFlota';
import AsignarFlota from './AsignarFlota';
import BackButton from '../../components/BackButton/BackButton';


export const CREAR_VEHICULO_FLOTA = 'CREAR_VEHICULO_FLOTA';
export const EDITAR_VEHICULO_FLOTA = 'EDITAR_VEHICULO_FLOTA';
export const ASIGNAR_VEHICULO_RUTA = 'ASIGNAR_VEHICULO_RUTA';
const MENSAJES_DE_RESPUESTA = {
    CREAR_VEHICULO_FLOTA: 'Vehiculo registrada correctamente',
    EDITAR_VEHICULO_FLOTA: 'Vehiculo actualizada correctamente',
    ASIGNAR_VEHICULO_RUTA: 'Vehiculo asignado correctamente'
};

export default function Flota() {
    var classes = useStyle();

    const { flotaId } = useParams();
    const [snack, setSnack] = useState({});
    const [openDialog, setOpenDialog] = useState(false);
    const [formulario, setFormulario] = useState(CREAR_VEHICULO_FLOTA);
    const [initialValues, setInitialValues] = useState({});
    const [flotas, setFlotas] = useState([]);

    useEffect(() => {
        const getFlotas = async () => {
            const flotas = await FlotaService.getAll(flotaId);
            setFlotas(flotas.data);
        };
        getFlotas();
    }, []);

    const columns = [
        {
            Header: 'Lista de Vehiculos de una empresa',
            columns: [
                {
                    Header: "#",
                    accessor: 'id',
                },
                {
                    Header: 'Placa Vehiculo',
                    accessor: 'placaVehiculo',
                },
                {
                    Header: 'Fecha Registro',
                    accessor: 'fechaRegistroVehiculo',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Estado',
                    accessor: 'estadoVehiculo',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: 'Codigo SOAT',
                    accessor: 'codigoSoat',
                    alignBody: 'center',
                    alignHeader: 'center'
                },
                {
                    Header: "Acciones",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const flota = flotas[rowIdx];

                        return (
                            <>
                                <Stack spacing={2} direction="row">
                                    <Button variant="contained" size="small" onClick={() => {
                                        setInitialValues(flota);
                                        setFormulario(EDITAR_VEHICULO_FLOTA);
                                        setOpenDialog(true);
                                    }}>
                                        Editar
                                    </Button>
                                    <Button variant="contained" size="small" onClick={() => {
                                        setInitialValues(flota);
                                        setFormulario(ASIGNAR_VEHICULO_RUTA);
                                        setOpenDialog(true);
                                    }}>
                                        Asignar Ruta
                                    </Button>
                                </Stack>
                            </>
                        );
                    }
                }
            ],
        },
    ];

    const onSuccess = (flota) => {
        if (formulario === CREAR_VEHICULO_FLOTA)
            setFlotas([...flotas, ...flota]);

        if (formulario === EDITAR_VEHICULO_FLOTA) {
            const nFlotas = flotas.map(f => f.placaVehiculo === flota.placaVehiculo ? { ...f, ...flota } : f);
            setFlotas(nFlotas);
            setInitialValues({});
        }

        setOpenDialog(false);
        setSnack({ ...snack, open: true, severity: 'success', message: MENSAJES_DE_RESPUESTA[formulario] });
    }

    const onCloseDialog = () => {
        setInitialValues({});
        setOpenDialog(false);
    }

    return (
        <>
            <BackButton to='../empresas' />
            <Notification snack={snack} setSnack={setSnack} />
            <Stack direction="row" spacing={1} style={{ float: 'right' }}>
                <Button variant="outlined" onClick={() => {
                    setFormulario(CREAR_VEHICULO_FLOTA);
                    setOpenDialog(true);
                    setInitialValues({ "idEtt": flotaId });
                }}>Registrar Vehiculo</Button>
            </Stack>

            {formulario === ASIGNAR_VEHICULO_RUTA ?
                <Dialog open={openDialog} maxWidth="lg" title={formulario === ASIGNAR_VEHICULO_RUTA ? 'Asignar ruta' : ''} handleClose={onCloseDialog}>
                    <AsignarFlota
                        formulario={formulario}
                        initialValues={initialValues}
                        onSuccess={(flota) => {
                            onSuccess(flota);
                        }}
                        onError={(error) => {
                            setSnack({ ...snack, open: true, severity: 'error', message: `Ocurrió un error registrando la flota: ${error}` });
                            setOpenDialog(false);
                        }}
                    />
                </Dialog> :
                <Dialog open={openDialog} title={formulario === CREAR_VEHICULO_FLOTA ? 'Registrar Vehiculo' : formulario === EDITAR_VEHICULO_FLOTA ? 'Editar vehiculo' : 'Asignar ruta'} handleClose={onCloseDialog}>
                    <CrearFlota
                        formulario={formulario}
                        initialValues={initialValues}
                        onSuccess={(flota) => {
                            onSuccess(flota);
                        }}
                        onError={(error) => {
                            setSnack({ ...snack, open: true, severity: 'error', message: `Ocurrió un error registrando la flota: ${error}` });
                            setOpenDialog(false);
                        }}
                    />
                </Dialog>
            }
            <BasicTable
                columns={columns}
                data={flotas}
                className={classes.container}
            />
        </>
    );
}