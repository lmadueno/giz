import { useFormik } from 'formik';

import * as yup from 'yup';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

import FlotaService from '../../services/FlotaService';
import { CREAR_VEHICULO_FLOTA } from './Flotas';

const validationSchema = yup.object({
    placaVehiculo: yup
        .string('Ingresa la placa del vehiculo')
        .required('Placa del vehiculo es requerido'),
    idEstadoVehiculo: yup
        .string('Debe seleccionar el estado del vehiculo')
        .required('Estado del vehiculo es requerido'),
});

export default function CrearFlota({ formulario, initialValues, onSuccess, onError }) {
    const formik = useFormik({
        initialValues: {
            idEtt: initialValues?.idEtt || '',
            placaVehiculo: initialValues?.placaVehiculo || '',
            codigoSoat: initialValues?.codigoSoat || '',
            afabricacionVehiculo: initialValues?.afabricacionVehiculo || '',
            idTipoVehiculo: initialValues?.idTipoVehiculo || '',
            idEstadoVehiculo: initialValues?.idEstadoVehiculo || '',
            vencimientoSoat: initialValues?.vencimientoSoat || '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            postFlota(values);
        },
    });

    const postFlota = async (body) => {
        try {
            const res = formulario === CREAR_VEHICULO_FLOTA ? await FlotaService.post(body) : await FlotaService.put(body);
            onSuccess(res.data);
        } catch (error) {
            onError(error);
        }
    }

    return (
        <>
            <form onSubmit={formik.handleSubmit}>
                <TextField
                    autoFocus
                    fullWidth
                    disabled={formulario === CREAR_VEHICULO_FLOTA ? false : true}
                    margin="dense"
                    id="placaVehiculo"
                    name="placaVehiculo"
                    label="Placa Vehículo"
                    value={formik.values.placaVehiculo}
                    onChange={formik.handleChange}
                    error={formik.touched.placaVehiculo && Boolean(formik.errors.placaVehiculo)}
                    helperText={formik.touched.placaVehiculo && formik.errors.placaVehiculo}
                />
                <TextField
                    fullWidth
                    margin="dense"
                    id="codigoSoat"
                    name="codigoSoat"
                    label="Cod. Soat"
                    value={formik.values.codigoSoat}
                    onChange={formik.handleChange}
                    error={formik.touched.codigoSoat && Boolean(formik.errors.codigoSoat)}
                    helperText={formik.touched.codigoSoat && formik.errors.codigoSoat}
                />
                <TextField
                    fullWidth
                    margin="dense"
                    id="afabricacionVehiculo"
                    name="afabricacionVehiculo"
                    label="Año Fab."
                    value={formik.values.afabricacionVehiculo}
                    onChange={formik.handleChange}
                    error={formik.touched.afabricacionVehiculo && Boolean(formik.errors.afabricacionVehiculo)}
                    helperText={formik.touched.afabricacionVehiculo && formik.errors.afabricacionVehiculo}
                />
                <TextField
                    fullWidth
                    margin="dense"
                    id="vencimientoSoat"
                    name="vencimientoSoat"
                    label="SOAT FV"
                    value={formik.values.vencimientoSoat}
                    onChange={formik.handleChange}
                    error={formik.touched.vencimientoSoat && Boolean(formik.errors.vencimientoSoat)}
                    helperText={formik.touched.vencimientoSoat && formik.errors.vencimientoSoat}
                />
                <FormControl fullWidth margin="dense">
                    <InputLabel id="eVehiculo" >Estado Vehículo</InputLabel>
                    <Select
                        defaultValue={formik.values.idEstadoVehiculo}
                        labelId="eVehiculo"
                        id="idEstadoVehiculo"
                        name="idEstadoVehiculo"
                        label="Estadp Vehículo"
                        onChange={formik.handleChange}
                        error={formik.touched.idEstadoVehiculo && Boolean(formik.errors.idEstadoVehiculo)}
                        helperText={formik.touched.idEstadoVehiculo && formik.errors.idEstadoVehiculo}
                    >
                        <MenuItem value={1}>Activo</MenuItem>
                        <MenuItem value={2}>Inactivo</MenuItem>
                    </Select>

                </FormControl>
                <FormControl fullWidth margin="dense">
                    <InputLabel id="tVehiculo">Tipo Vehículo</InputLabel>
                    <Select
                        defaultValue={formik.values.idTipoVehiculo}
                        labelId="tVehiculo"
                        id="idTipoVehiculo"
                        name="idTipoVehiculo"
                        label="Tipo Vehículo"
                        onChange={formik.handleChange}
                        error={formik.touched.idTipoVehiculo && Boolean(formik.errors.idTipoVehiculo)}
                        helperText={formik.touched.idTipoVehiculo && formik.errors.idTipoVehiculo}
                    >
                        <MenuItem value={1}>Combi</MenuItem>
                        <MenuItem value={2}>Bus</MenuItem>
                        <MenuItem value={3}>Metro</MenuItem>
                        <MenuItem value={4}>Couster</MenuItem>
                        <MenuItem value={5}>Colectivo</MenuItem>
                    </Select>
                </FormControl>
                <Button color="primary" variant="contained" fullWidth type="submit">
                    Guardar
                </Button>
            </form>
        </>
    );
}