import { useParams } from 'react-router-dom';
import Consulta1 from "./Consulta1/Consulta1";
import Consulta3 from "./Consulta3/Consulta3";
import Consulta4 from "./Consulta4/Consulta4";
import Consulta5 from "./Consulta5/Consulta5";
import Consulta10 from "./Consulta10/Consulta10";
import Consulta11 from "./Consulta11/Consulta11";
import Consulta16 from "./Consulta16/Consulta16";
import Consulta19 from "./Consulta19/Consulta19";
import Consulta21 from "./Consulta21/Consulta21";
import Consulta24 from "./Consulta24/Consulta24";

const Components = {
    consulta1: Consulta1,
    consulta3: Consulta3,
    consulta4: Consulta4,
    consulta5: Consulta5,
    consulta10: Consulta10,
    consulta11: Consulta11,
    consulta16: Consulta16,
    consulta19: Consulta19,
    consulta21: Consulta21,
    consulta24: Consulta24
};

export default function Consulta() {
    const { consultaId } = useParams();
    const cName = `consulta${consultaId}`;
    const Component = Components[cName];

    if (Component)
        return <Component consultaId={consultaId} />

    if (!Component)
        return <h3>Consulta pendiente de desarrollo</h3>
}