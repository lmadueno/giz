import { useState, useEffect } from "react";

import { Link } from "react-router-dom";
import Button from '@mui/material/Button';
import BasicTable from '../../components/Table/Table';

import ReporteService from "../../services/ReporteService";

export default function Reportes() {

    const [reportes, setReportes] = useState([]);

    useEffect(() => {
        const getReportes = async () => {
            const reportes = await ReporteService.getAll();
            setReportes(reportes.data);
        };
        getReportes();
    }, []);

    const columns = [
        {
            Header: 'Reportes',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: 'Reporte',
                    accessor: 'name',
                },
                {
                    Header: "Visualizar",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id } = reportes[rowIdx];

                        return (
                            <Button component={Link} to={`/reportes/${id}`} variant="contained" size="small">
                                Ver
                            </Button>
                        );
                    }
                }
            ],
        },
    ];

    return (
        <>
            <BasicTable
                isExportable={false}
                columns={columns}
                data={reportes}
                sizePro='small'
            />
        </>
    );
}