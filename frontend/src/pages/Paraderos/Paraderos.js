import { LayersControl, LayerGroup } from '@monsonjeremy/react-leaflet';
import Map from '../../components/Map/Map';
import ParaderosMarkers from '../../components/Poligonos/ParaderosMarkers';
import RutasPoligono from '../../components/Poligonos/RutasPoligono';
import HexagonosPoligono from '../../components/Poligonos/HexagonosPoligono';

export default function Paraderos() {
    return (
        <Map>
            <LayersControl position="topright">
                <LayersControl.Overlay name="Paraderos" checked>
                    <LayerGroup>
                        <ParaderosMarkers />
                    </LayerGroup>
                </LayersControl.Overlay>
                <LayersControl.Overlay name="Rutas" checked>
                    <LayerGroup>
                        <RutasPoligono />
                    </LayerGroup>
                </LayersControl.Overlay>
                {/*<LayersControl.Overlay name="Hexagonos">
                    <LayerGroup>
                        <HexagonosPoligono />
                    </LayerGroup>
                </LayersControl.Overlay>*/}
            </LayersControl>
        </Map>
    );
}