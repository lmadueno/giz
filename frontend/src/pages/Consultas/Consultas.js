import { useState, useEffect } from "react";

import { Link } from "react-router-dom";
import Button from '@mui/material/Button';
import BasicTable from '../../components/Table/Table';

import ConsultaService from "../../services/ConsultaService";

export default function Consultas() {

    const [consultas, setConsultas] = useState([]);

    useEffect(() => {
        const getConsultas = async () => {
            const consultas = await ConsultaService.getAll();
            setConsultas(consultas.data);
        };
        getConsultas();
    }, []);

    const columns = [
        {
            Header: 'Consulta',
            columns: [
                {
                    Header: '#',
                    accessor: 'id',
                },
                {
                    Header: 'Consulta',
                    accessor: 'name',
                },
                {
                    Header: "Visualizar",
                    Cell: (props) => {
                        const rowIdx = props.row.id;
                        const { id } = consultas[rowIdx];

                        return (
                            <Button component={Link} to={`/consultas/${id}`} variant="contained" size="small">
                                Ver
                            </Button>
                        );
                    }
                }
            ],
        },
    ];

    return (
        <>
            <BasicTable
           
                columns={columns}
                data={consultas}
                sizePro='small'
            />
        </>
    );
}