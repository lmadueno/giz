import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Grid, Typography, TextField, Button } from '@mui/material';
import SesionService from '../../services/SesionService';

export default function Login() {
    const [usuario, setUsuario] = useState();
    const [contrasena, setContrasena] = useState();
    const history = useHistory();

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            await SesionService.login({
                usuario,
                contrasena
            });
            history.push('/');
        } catch (error) {
            console.error('error', error);
        }
    }

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: 'calc(100vh - 100px)' }
            }
        >
            <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column', maxWidth: 500, minWidth: 300, }}>
                <div style={{ marginBottom: 10, textAlign: 'center' }}>
                    <img src="/images/giz.png" className="App-logo" alt="logo" style={{ marginRight: 5 }} />
                </div>
                <Typography style={{ textAlign: 'center', fontWeight: 'bolder', marginBottom: 15 }} variant='h5' >
                    Inicio de sesión
                </Typography>
                <Typography style={{ textAlign: 'center', marginBottom: 15 }} variant='h6' >
                    Ingresa tu usuario y contraseña
                </Typography>
                <TextField autoFocus name="usuario" required label="Usuario" margin="normal" variant="outlined" onChange={e => setUsuario(e.target.value)} />
                <TextField name="contrasenia" required label="Contraseña" margin="normal" type="password" variant="outlined" onChange={e => setContrasena(e.target.value)} />
                <Button type="submit" color="primary" variant="contained" size="large">Iniciar sesión</Button>
            </form>
        </Grid>
    );
}