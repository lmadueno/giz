import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import ArrowBack from '@mui/icons-material/ArrowBack';
import { useHistory } from "react-router-dom";

export default function BackButton({ handleClickBack, to }) {
    let history = useHistory();

    const handleClickBackTo = () => {
        history.push(to);
    }

    if (handleClickBack)
        return (
            <>
                <Stack direction="row" spacing={1} style={{ float: 'left' }}>
                    <Button variant="contained" startIcon={<ArrowBack />} onClick={handleClickBack}>
                        Volver
                    </Button>
                </Stack>
            </>
        )

    if (to)
        return (
            <>
                <Stack direction="row" spacing={1} style={{ float: 'left' }}>
                    <Button variant="contained" startIcon={<ArrowBack />} onClick={handleClickBackTo}>
                        Volver
                    </Button>
                </Stack>
            </>
        )
}