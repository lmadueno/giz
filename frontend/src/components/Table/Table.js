import React from 'react';
import { useExportData } from "react-table-plugins";
import XLSX from "xlsx";
import JsPDF from "jspdf";
import "jspdf-autotable";
import MaUTable from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableExport from '../TableExport/TableExport';
import { useTable } from 'react-table';

const DEFAULT_SIZE_TABLE = 'small';

export default function BasicTable({ isExportable = false, sizePro = DEFAULT_SIZE_TABLE, columns, data, className }) {
    const { getTableProps, headerGroups, rows, prepareRow, exportData } = useTable(
        {
            columns,
            data,
            getExportFileBlob
        },
        useExportData
    );

    function getExportFileBlob({ columns, data, fileType, fileName }) {
        if (fileType === "xlsx") {
            // XLSX
            const header = columns
                .filter((c) => c.Header !== "Action")
                .map((c) => c.exportValue);
            const compatibleData = data.map((row) => {
                const obj = {};
                header.forEach((col, index) => {
                    obj[col] = row[index];
                });
                return obj;
            });

            let wb = XLSX.utils.book_new();
            let ws1 = XLSX.utils.json_to_sheet(compatibleData, {
                header
            });
            XLSX.utils.book_append_sheet(wb, ws1, "React Table Data");
            XLSX.writeFile(wb, `${fileName}.xlsx`);

            return false;
        }

        //PDF
        if (fileType === "pdf") {
            const headerNames = columns
                .filter((c) => c.Header !== "Action")
                .map((column) => column.exportValue);
            const doc = new JsPDF();
            doc.autoTable({
                head: [headerNames],
                body: data,
                styles: {
                    minCellHeight: 9,
                    halign: "left",
                    valign: "center",
                    fontSize: 11
                }
            });
            doc.save(`${fileName}.pdf`);

            return false;
        }

        return false;
    }

    return (
        <>
            {isExportable &&
                <TableExport onClickPDF={() => exportData("pdf", true)} onClickExcel={() => exportData("xlsx", true)} />
            }
            <br/>
            <br/>
            <MaUTable {...getTableProps()} className={className} size={sizePro}>
                <TableHead>
                    {headerGroups.map(headerGroup => (
                        <TableRow {...headerGroup.getHeaderGroupProps()} align={headerGroup.alignHeader || 'center'}>
                            {headerGroup.headers.map(column => (
                                <TableCell {...column.getHeaderProps()} align={column.alignHeader || 'left'}>
                                    {column.render('Header')}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody>
                    {rows.map((row, i) => {
                        prepareRow(row)
                        return (
                            <TableRow {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <TableCell {...cell.getCellProps()} align={cell.column.alignBody || 'left'}>
                                            {cell.render('Cell')}
                                        </TableCell>
                                    )
                                })}
                            </TableRow>
                        )
                    })}
                </TableBody>
            </MaUTable>
        </>
    )
}