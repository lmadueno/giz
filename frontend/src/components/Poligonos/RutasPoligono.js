import React, { useState, useEffect } from 'react';
import { GeoJSON } from '@monsonjeremy/react-leaflet';
import RutaService from '../../services/RutaService';

export const STYLE_GEOM_2_1 = {
    fillColor: "transparent",
    color: "#3388ff",
};

export const STYLE_GEOM_1_2 = {
    fillColor: "transparent",
    color: "#E53D00",
};

export default function RutasPoligono() {
    const [rutas, setRutas] = useState([]);

    useEffect(() => {
        const getRutas = async () => {
            const rutas = await RutaService.getAll();
            setRutas(rutas.data);
        };
        getRutas();
    }, []);

    return (
        <>
            {
                rutas.map((ruta, index) => (
                    <React.Fragment key={index}>
                        <GeoJSON data={ruta.geom12} style={STYLE_GEOM_1_2}></GeoJSON>
                        <GeoJSON data={ruta.geom21} style={STYLE_GEOM_2_1}></GeoJSON>
                    </React.Fragment>
                ))
            }
        </>
    );
}