import { useState, useEffect } from 'react';
import Marker from '../Marker/Marker';
import ParaderoService from '../../services/ParaderoService';

export default function ParaderosMarkers() {
    const [paraderos, setParaderos] = useState([]);

    useEffect(() => {
        const getParaderos = async () => {
            const paraderos = await ParaderoService.getAll();
            setParaderos(paraderos.data);
        };
        getParaderos();
    }, []);

    return (
        <>
            {
                paraderos.map((paradero, index) => (
                    <Marker key={index} position={[paradero.latitud, paradero.longitud]} />
                ))
            }
        </>
    );
}