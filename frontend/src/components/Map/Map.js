import { MapContainer, TileLayer } from '@monsonjeremy/react-leaflet';
import "leaflet/dist/leaflet.css";

export default function Map({ children, width = '100%', height = '800px' }) {
    const position = [-16.40732281908651, -71.53626778407354];
    const mapStyle = {
        width: width,
        height: height,
    };

    return (
        <MapContainer center={position} zoom={13} scrollWheelZoom={true} style={mapStyle}>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {children}
        </MapContainer>
    );
}