import React from 'react';
import { Switch, Route, withRouter } from "react-router-dom";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';

//Components
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

//Pages
import Inicio from '../../pages/Inicio/Inicio';
import Login from '../../pages/Login/Login';
import Consultas from '../../pages/Consultas/Consultas';
import Consulta from '../../pages/Consulta';
import Reportes from '../../pages/Reportes/Reportes';
import Reporte from '../../pages/Reporte';
import Alertas from '../../pages/Alertas/Alertas';
import AlertasAdmin from '../../pages/Alertas/AlertasAdmin';
import Empresas from '../../pages/Empresas/Empresas';
import Flotas from '../../pages/Flota/Flotas';
import Paraderos from '../../pages/Paraderos/Paraderos';
import Parametros from '../../pages/Parametros/Parametros';
import Emv from '../../pages/Emv/Emv';
import Rutas from '../../pages/Rutas/Rutas';
import Seguridad from '../../pages/Seguridad/Seguridad';

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

const Layout = () => {
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <Header handleDrawerOpen={handleDrawerOpen} />
                <Sidebar open={open} handleDrawerClose={handleDrawerClose} drawerWidth={drawerWidth} />

                <Main open={open} style={{ height: '100vh' }}>
                    <DrawerHeader />
                    <Switch>
                        <Route path="/login" component={Login} />
                        <Route path='/' exact component={Inicio} />
                        <Route exact path="/consultas" component={Consultas} />
                        <Route exact path="/consultas/:consultaId" component={Consulta} />
                        <Route exact path="/reportes" component={Reportes} />
                        <Route exact path="/reportes/:reporteId" component={Reporte} />
                        <Route path="/alertas" component={Alertas} />
                        <Route path="/alertasAdmin" component={AlertasAdmin} />
                        <Route exact path="/empresas" component={Empresas} />
                        <Route exact path="/flotas/:flotaId" component={Flotas} />
                        <Route path="/paraderos" component={Paraderos} />
                        <Route path="/parametros" component={Parametros} />
                        <Route path="/emv" component={Emv} />
                        <Route path="/rutas" component={Rutas} />
                        <Route path="/seguridad" component={Seguridad} />
                    </Switch>
                </Main>
            </Box>
        </>
    );
};

//export default Layout;
export default withRouter(Layout);
