import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

export default function FormDialog({ open = false, maxWidth = 'sm', title, handleClose, children }) {
    return (
        <Dialog open={open} maxWidth={maxWidth} onClose={handleClose} >
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <div style={{ margin: '10px 0px' }}>
                    {children}
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancelar</Button>
            </DialogActions>
        </Dialog >
    );
}
