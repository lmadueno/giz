import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

export default function TableExport({ onClickPDF, onClickExcel }) {
    return (
        <>
            <Stack direction="row" spacing={1} style={{ float: 'right' }}>
                <Button variant="outlined" onClick={onClickPDF}>Exportar PDF</Button>
                <Button variant="outlined" onClick={onClickExcel}>Exportar Excel</Button>
            </Stack>
        </>
    )
}