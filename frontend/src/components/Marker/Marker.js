import { Marker as MapMarker, Popup } from '@monsonjeremy/react-leaflet';
import L from "leaflet";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

export default function Marker({ id = 0, position = [], children }) {
    return (
        <MapMarker position={position} eventHandlers={{
            click: () => {
                console.log('OnClick')
            },
        }}>
            {children}
            <Popup>
                id: {id}
            </Popup>
        </MapMarker>
    );
}