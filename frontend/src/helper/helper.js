export const jsonIsValid = jsonStr => {
    try {
        JSON.parse(jsonStr);
        return true;
    } catch (e) {
        return false;
    }
}