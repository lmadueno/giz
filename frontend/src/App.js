import { BrowserRouter as Router } from "react-router-dom";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { red } from '@mui/material/colors';
import Layout from "./components/Layout/Layout";

const theme = createTheme({
  palette: {
    primary: {
      main: red[800],
    },
    secondary: {
      main: red[800],
    },
  },
});

export function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Layout></Layout>
      </Router>
    </ThemeProvider>
  );
};

export default App;