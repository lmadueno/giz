import http from "./http-common";

const getAll = async () => {
    return http.get("/reportes");
};

const getEmpresas = async (reporteId = 0) => {
    return http.get(`/reportes/${reporteId}/empresas`);
};

const getRutas = async (reporteId = 0, empresaId = 0) => {
    return http.get(`/reportes/${reporteId}/empresas/${empresaId}/rutas`);
};

const getVehiculos = async (reporteId = 0, empresaId = 0, rutaId = 0) => {
    return http.get(`/reportes/${reporteId}/empresas/${empresaId}/rutas/${rutaId}/vehiculos`);
};

const ReporteService = {
    getAll,
    getEmpresas,
    getRutas,
    getVehiculos
};

export default ReporteService;