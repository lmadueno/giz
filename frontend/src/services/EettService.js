import http from "./http-common"

const getAll = async () => {
    return http.get("/eett");
};

const post = async (body) => {
    return http.post("/eett", body);
};

const put = async (body) => {
    return http.put(`/eett/${body.idEtt}`, body);
};


const EettService = {
    getAll,
    post,
    put
};

export default EettService;