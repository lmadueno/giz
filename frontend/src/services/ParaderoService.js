import http from "./http-common"

const getAll = async () => {
    return http.get("/paraderos");
};

const ParaderoService = {
    getAll,
};

export default ParaderoService;