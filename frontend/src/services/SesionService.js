import http from "./http-common";

const login = async (credenciales) => {
    const response = await http.post("/seguridad/iniciar-sesion", credenciales);
    const token = response.data.token;
    if (token) localStorage.setItem('user', JSON.stringify(response.data));
    return response.data;
};

export const isAuthenticated = () => {
    const user = localStorage.getItem('user');
    if (!user) {
        return {};
    }
    return JSON.parse(user);
};

const SesionService = {
    login,
    isAuthenticated
};

export default SesionService;