import http from "./http-common";

const getAll = async () => {
    return http.get("/consultas");
};

const getEmpresas = async (consultaId = 0) => {
    return http.get(`/consultas/${consultaId}/empresas`);
};

const getRutas = async (consultaId = 0, empresaId = 0) => {
    return http.get(`/consultas/${consultaId}/empresas/${empresaId}/rutas`);
};

const getVehiculos = async (consultaId = 0, empresaId = 0, rutaId = 0) => {
    return http.get(`/consultas/${consultaId}/empresas/${empresaId}/rutas/${rutaId}/vehiculos`);
};

const ConsultaService = {
    getAll,
    getEmpresas,
    getRutas,
    getVehiculos
};

export default ConsultaService;