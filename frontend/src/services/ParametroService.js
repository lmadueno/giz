import http from "./http-common"

const getAll = async () => {
    return http.get("/parametros");
};

const post = async (body) => {
    return http.post("/parametros", body);
};

const put = async (body) => {
    return http.put(`/parametros/${body.idParametro}`, body);
};

const ParametroService = {
    getAll,
    post,
    put
};

export default ParametroService;