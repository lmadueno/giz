import http from "./http-common"

const getAll = async () => {
    return http.get("/rutas");
};

const post = async (body) => {
    return http.post("/rutas", body);
};

const put = async (body) => {
    return http.put(`/rutas/${body.idRuta}`, body);
};

const RutaService = {
    getAll,
    post,
    put
};

export default RutaService;